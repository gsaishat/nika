var old_map_h;
var old_title;
var total;
var from;
var need_more;

function submitMap(cat_id){

    $('#filter_form input:checkbox').prop('checked', false);
    $('#filter_form input#cat'+cat_id).prop('checked', true);
    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('input:checkbox').prop('checked', true);

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').parent('ul').find('li ul').hide();

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('ul[rel='+cat_id+']').toggle();

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('input:checkbox').prop('checked', true);

    getMarkers();

}

function selectMapCat(cbox_obj, cat_id){

    var state = $(cbox_obj).prop('checked') ? true : false;

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('input:checkbox').prop('checked', state);

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('ul[rel='+cat_id+']').toggle();

    $('input#cat'+cat_id).parent('td').parent('tr').parent('tbody').parent('table').parent('li').find('input:checkbox').prop('checked', state);

}

function getPlaces(){
    getMarkers();
}

function generatePages(pages, perpage){

    $('#marker_pages_link').show();

    for(var p=1; p<=pages; p++){

        var f = (p-1) * perpage;
        var phtml = '<a class="page'+p+'" href="javascript:" onclick="getMarkers('+f+', '+p+')">'+p+'</a> ';

        $('#marker_pages').append(phtml);

    }

    $('#marker_pages .page1').addClass('active');

}

function getMarkers(from, page){

    if (!from) { from = 0; }
    if (!page) { page = 1; }

    $("form#filter_form input[name=from]").val(from);

    $('.marker_loading').show();

    if (from == 0){
        $('#marker_pages').html('');
        $('#marker_pages_link').hide();
    } else {
        $('#marker_pages a').removeClass('active');
        $('#marker_pages .page'+page).addClass('active');
    }

    old_title = 'Обновить карту';

    $('#refresh_btn').prop('disabled', true);
    $('#refresh_btn').val('Загрузка...');

    $("form#filter_form").ajaxSubmit({
          success: function(msg) {
              $('.marker_loading').hide();
              clearMap();
              if (msg){
                  eval(msg);
                  addMarkers(markers_list);
                  if (pages > 1 && from == 0){
                        generatePages(pages, perpage);
                  }
              }
              $('#refresh_btn').prop('disabled', false);
              $('#refresh_btn').val(old_title);
          }
    });

}

function toggleMapSize(map_div_id){

    if (map_div_id != '#placemap') { map_div_id = '#citymap'; }

    var is_fullscreen = $('#map_wrapper').hasClass('fullscreen_map');

    if (!is_fullscreen){
        old_map_wrap_h  = $('#map_wrapper').height();
        old_map_h       = $(map_div_id).height();
        old_map_w       = $('#map_wrapper').width();
    }

    $('#map_wrapper').toggleClass('fullscreen_map');

    is_fullscreen = !is_fullscreen;

    if (is_fullscreen){

        var screen_h    = $(window).height();

        $('#map_wrapper').height(screen_h+'px');
        $('#map_wrapper').width('100%');
        $(map_div_id).height('100%');

        $('#fullscreen_link a').removeClass('maximize_button').addClass('minimize_button').html('Свернуть');

        window.location.href = '#';

    } else {

        $('#fullscreen_link a').removeClass('minimize_button').addClass('maximize_button').html('На весь экран');

        $('#map_wrapper').height(old_map_wrap_h+'px');
        $(map_div_id).height(old_map_h+'px');
        $('#map_wrapper').width(old_map_w+'px');

    }

    redrawMap();

}

