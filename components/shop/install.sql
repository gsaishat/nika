DROP TABLE IF EXISTS `#__shop_cart`;

CREATE TABLE `#__shop_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL,
  `item_id` int(11) NOT NULL,
  `var_art_no` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `pubdate` datetime NOT NULL,
  `chars` text,
  `chars_hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `chars_hash` (`chars_hash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_cats`;

CREATE TABLE `#__shop_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `seolink` varchar(100) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '1',
  `NSLeft` int(11) NOT NULL,
  `NSRight` int(11) NOT NULL,
  `NSLevel` int(11) NOT NULL,
  `NSDiffer` int(11) NOT NULL,
  `NSIgnore` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `config` text NOT NULL,
  `tpl` varchar(50) NOT NULL DEFAULT 'com_inshop_view.tpl',
  `url` varchar(200) NOT NULL,
  `is_catalog` tinyint(4) NOT NULL DEFAULT '0',
  `meta_desc` varchar(255) NOT NULL,
  `meta_keys` varchar(255) NOT NULL,
  `pagetitle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seolink` (`seolink`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_cats` (`id`, `parent_id`, `title`, `description`, `seolink`, `published`, `NSLeft`, `NSRight`, `NSLevel`, `NSDiffer`, `NSIgnore`, `ordering`, `config`, `tpl`, `url`, `is_catalog`, `meta_desc`, `meta_keys`, `pagetitle`) VALUES
(1, 0, '-- Корневая категория --', '', '', 1, 1, 22, 0, 0, 0, 1, '', 'com_inshop_view.tpl', '', 0, '', '', ''),
(19, 14, 'Пылесосы', '', 'bytovaja-tehnika/pylesosy', 1, 5, 6, 2, 0, 0, 3, '', 'com_inshop_view.tpl', '', 0, '', '', ''),
(18, 14, 'Стиральные машины', '', 'bytovaja-tehnika/stiralnye-mashiny', 1, 3, 4, 2, 0, 0, 2, '---\nicon: shop_category18.png\n', 'com_inshop_view.tpl', '', 0, 'Описание стиральных машин', 'Машинки, стиральные', 'Стиралки'),
(17, 15, 'MP3-плееры', '', 'bytovaja-tehnika/audio-i-video/MP3-pleery', 1, 14, 15, 3, 0, 0, 10, '---\nicon: shop_category17.\n', 'com_inshop_view.tpl', '', 0, '', '', ''),
(14, 1, 'Бытовая техника', '', 'bytovaja-tehnika', 1, 2, 17, 1, 0, 0, 10, '---\nicon: shop_category14.\n', 'com_inshop_view.tpl', '', 0, '', '', ''),
(15, 14, 'Аудио и видео', '', 'bytovaja-tehnika/audio-i-video', 1, 11, 16, 2, 0, 0, 7, '---\nicon: shop_category15.\n', 'com_inshop_view.tpl', '', 0, '', '', ''),
(16, 15, 'Видео-камеры', '', 'bytovaja-tehnika/audio-i-video/video-kamery', 1, 12, 13, 3, 0, 0, 9, '---\nicon: shop_category16.\n', 'com_inshop_view.tpl', '', 0, '', '', ''),
(20, 14, 'Микроволновые печи', '', 'bytovaja-tehnika/mikrovolnovye-pechi', 1, 7, 8, 2, 0, 0, 4, '', 'com_inshop_view.tpl', '', 0, '', '', ''),
(21, 14, 'Утюги', '', 'bytovaja-tehnika/utyugi', 1, 9, 10, 2, 0, 0, 5, '', 'com_inshop_view.tpl', '', 0, '', '', ''),
(22, 1, 'Программное обеспечение', '', 'programmnoe-obespechenie', 1, 18, 19, 1, 0, 0, 11, '---\nicon: shop_category22.\n', 'com_inshop_view.tpl', '', 0, '', '', ''),
(23, 1, 'Футболки', '', 'futbolki', 1, 20, 21, 1, 0, 0, 12, '---\nicon: shop_category23.\n', 'com_inshop_view.tpl', '', 0, '', '', '');

DROP TABLE IF EXISTS `#__shop_chars`;

CREATE TABLE `#__shop_chars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `published` tinyint(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  `fieldtype` varchar(10) NOT NULL,
  `fieldgroup` varchar(250) NOT NULL,
  `is_compare` tinyint(4) NOT NULL,
  `is_filter` tinyint(4) NOT NULL,
  `values` text NOT NULL,
  `bind_all` tinyint(4) NOT NULL,
  `ordering` smallint(6) NOT NULL,
  `is_filter_many` int(11) NOT NULL DEFAULT '0',
  `is_custom` tinyint(4) NOT NULL DEFAULT '0',
  `units` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`is_compare`,`is_filter`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_chars` (`id`, `published`, `title`, `fieldtype`, `fieldgroup`, `is_compare`, `is_filter`, `values`, `bind_all`, `ordering`, `is_filter_many`, `is_custom`, `units`) VALUES
(19, 1, 'Гарантия', 'text', 'Общие', 0, 0, 'Нет\r\n1 год\r\n2 года\r\n3 года', 1, 0, 0, 0, NULL),
(8, 1, 'Тип загрузки', 'text', '', 1, 1, 'Фронтальная\r\nВертикальная', 0, 0, 0, 0, NULL),
(9, 1, 'Сушка', 'text', '', 1, 1, 'Да\r\nНет', 0, 0, 0, 0, NULL),
(10, 1, 'Оптический Zoom', 'text', 'Объектив', 0, 1, '', 0, 0, 0, 0, NULL),
(11, 1, 'Цифровой Zoom', 'text', 'Объектив', 1, 1, '', 0, 0, 0, 0, NULL),
(12, 1, 'Разрешение дисплея', 'text', '', 0, 1, '', 0, 0, 0, 0, NULL),
(13, 1, 'Встроенная память', 'int', '', 0, 1, '', 0, 0, 0, 0, 'Гб'),
(14, 1, 'Цифровой тюнер', 'text', '', 1, 1, 'FM\r\nFM/AM\r\nНет', 0, 0, 0, 0, NULL),
(15, 1, 'Работа от батареи', 'text', '', 0, 1, '', 0, 0, 0, 0, NULL),
(16, 1, 'Воспроизв. видео файлов', 'text', '', 1, 1, 'Да\r\nНет', 0, 0, 0, 0, NULL),
(17, 1, 'Требования к серверу', 'text', 'Общие', 1, 0, '', 0, 0, 0, 0, NULL),
(18, 1, 'Техническая поддержка', 'link', 'Общие', 1, 1, 'Да\r\nНет', 0, 0, 0, 0, NULL),
(20, 1, 'Сайт производителя', 'link', 'Общие', 0, 0, '', 1, 0, 0, 0, NULL),
(21, 1, 'Файл', 'file', '', 1, 1, '', 0, 0, 1, 0, NULL),
(22, 1, 'Габариты', 'text', 'Общие', 1, 1, '', 1, 0, 0, 0, NULL),
(23, 1, 'Вес', 'text', 'Общие', 1, 1, '', 1, 0, 0, 0, NULL),
(24, 1, 'Материал', 'text', '', 1, 1, 'Синтетика\r\nХлопок', 0, 0, 0, 0, NULL),
(25, 1, 'Размер', 'cbox', '', 1, 1, 'S\r\nM\r\nL\r\nXL\r\nXXL', 0, 0, 1, 1, NULL),
(26, 1, 'Цвет', 'cbox', '', 1, 1, 'Белый\r\nЧерный\r\nКрасный\r\nСиний', 0, 0, 1, 1, NULL),
(27, 1, 'Картинка', 'text', '', 0, 0, '', 1, 0, 0, 0, NULL);

DROP TABLE IF EXISTS `#__shop_chars_bind`;

CREATE TABLE `#__shop_chars_bind` (
  `char_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `ordering` smallint(6) NOT NULL,
  KEY `char_id` (`char_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_chars_bind` (`char_id`, `cat_id`, `ordering`) VALUES
(10, 16, 2),
(11, 16, 3),
(12, 16, 1),
(13, 17, 1),
(13, 16, 4),
(14, 17, 4),
(15, 17, 3),
(16, 17, 2),
(17, 22, 1),
(18, 20, 1),
(18, 22, 2),
(21, 22, 3),
(9, 18, 1),
(8, 18, 2),
(24, 23, 1),
(25, 23, 2),
(26, 23, 3),
(13, 15, 1);

DROP TABLE IF EXISTS `#__shop_chars_val`;

CREATE TABLE `#__shop_chars_val` (
  `item_id` int(11) NOT NULL,
  `char_id` int(11) NOT NULL,
  `val` varchar(255) NOT NULL,
  KEY `item_id` (`item_id`,`char_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_chars_val` (`item_id`, `char_id`, `val`) VALUES
(3, 15, 'до 12 часов'),
(2, 10, '60'),
(3, 19, '2 года'),
(4, 19, 'Нет'),
(2, 11, '200'),
(4, 13, '2'),
(4, 14, 'FM'),
(5, 19, 'Нет'),
(6, 19, '1 год'),
(2, 12, '641x480'),
(8, 19, 'Нет'),
(2, 19, 'Нет'),
(3, 16, 'Нет'),
(3, 13, '2'),
(4, 15, 'до 8 часов'),
(4, 16, 'Нет'),
(6, 8, 'Фронтальная'),
(5, 20, 'http://www.bosch.ru'),
(2, 13, '60'),
(6, 9, 'Да'),
(8, 17, 'Apache, PHP 5, MySQL 5'),
(3, 14, 'FM'),
(8, 18, 'Да'),
(5, 8, 'Фронтальная'),
(5, 9, 'Нет'),
(2, 20, 'http://'),
(4, 20, 'http://'),
(6, 20, 'http://test.ru'),
(5, 22, '100x150x300'),
(5, 23, '50 кг'),
(8, 20, 'qwe'),
(8, 22, ''),
(8, 23, ''),
(6, 22, ''),
(6, 23, ''),
(2, 22, ''),
(2, 23, ''),
(4, 22, ''),
(4, 23, ''),
(3, 20, 'http://'),
(3, 22, ''),
(3, 23, ''),
(10, 24, 'Хлопок'),
(10, 23, ''),
(10, 22, ''),
(10, 19, 'Нет'),
(10, 20, 'http://'),
(11, 19, 'Нет'),
(11, 20, 'http://'),
(11, 22, ''),
(11, 23, ''),
(11, 24, 'Синтетика'),
(10, 25, '|M|L|XL|'),
(10, 26, '|Белый|Черный|'),
(11, 25, '|S|XXL|'),
(11, 26, '|Белый|Красный|'),
(8, 27, '/images/rondinara-beach.jpg'),
(5, 27, ''),
(10, 27, ''),
(11, 27, ''),
(3, 27, ''),
(2, 27, ''),
(4, 27, ''),
(8, 21, '');

DROP TABLE IF EXISTS `#__shop_compare`;

CREATE TABLE `#__shop_compare` (
  `session_id` varchar(32) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pubdate` datetime NOT NULL,
  KEY `session_id` (`session_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_customers_data`;

CREATE TABLE `#__shop_customers_data` (
  `user_id` int(11) NOT NULL,
  `data` text NOT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_delivery`;

CREATE TABLE `#__shop_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `published` tinyint(4) NOT NULL,
  `minsumm` float NOT NULL,
  `freesumm` float NOT NULL,
  `price` float NOT NULL,
  `nofree` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_delivery` (`id`, `title`, `description`, `published`, `minsumm`, `freesumm`, `price`, `nofree`) VALUES
(1, 'Самовывоз из офиса', 'Приезжаете, забираете товар.', 1, 0, 0, 0, 0),
(2, 'Доставка курьером', 'Наш человек сам привезет товар.<br />\r\nВам нужно будет только выйти и встретить.', 1, 0, 0, 250, 1);

DROP TABLE IF EXISTS `#__shop_discounts`;

CREATE TABLE `#__shop_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `sign` tinyint(4) NOT NULL DEFAULT '-1',
  `groups` text NOT NULL,
  `cats` text NOT NULL,
  `amount` float NOT NULL,
  `is_percent` tinyint(4) NOT NULL,
  `is_forever` tinyint(4) NOT NULL,
  `date_until` date NOT NULL,
  `auto_delete` tinyint(4) NOT NULL,
  `published` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_until` (`date_until`),
  KEY `auto_delete` (`auto_delete`),
  KEY `published` (`published`),
  KEY `is_forever` (`is_forever`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_items`;

CREATE TABLE `#__shop_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` mediumint(9) NOT NULL,
  `vendor_id` mediumint(9) NOT NULL,
  `art_no` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `shortdesc` text NOT NULL,
  `description` longtext NOT NULL,
  `metakeys` varchar(250) NOT NULL,
  `metadesc` varchar(250) NOT NULL,
  `price` float NOT NULL,
  `old_price` float NOT NULL,
  `published` tinyint(4) NOT NULL,
  `pubdate` date NOT NULL,
  `is_hit` tinyint(4) NOT NULL,
  `is_front` tinyint(4) NOT NULL,
  `is_digital` tinyint(4) NOT NULL,
  `seolink` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `img_count` tinyint(4) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `filename_orig` varchar(250) NOT NULL,
  `filesize` int(11) NOT NULL,
  `filedate` date NOT NULL,
  `hits` int(11) NOT NULL,
  `tpl` varchar(50) NOT NULL DEFAULT 'com_inshop_item.tpl',
  `url` varchar(200) NOT NULL,
  `rating` float NOT NULL DEFAULT '0',
  `rating_votes` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`,`vendor_id`),
  KEY `art_no` (`art_no`),
  KEY `seolink` (`seolink`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_items` (`id`, `category_id`, `vendor_id`, `art_no`, `title`, `shortdesc`, `description`, `metakeys`, `metadesc`, `price`, `old_price`, `published`, `pubdate`, `is_hit`, `is_front`, `is_digital`, `seolink`, `qty`, `img_count`, `filename`, `filename_orig`, `filesize`, `filedate`, `hits`, `tpl`, `url`, `rating`, `rating_votes`) VALUES
(2, 16, 3, '0001', 'Sony DCR-SR47E', '<p>Сони - этим всё сказано. удобно лежит в руке, правильный вес, 95 минут съёмки со стандартной батареей, 1000 часов съёмки в HQ  Качество нормальное, не HD но для 81см телефизора выглядит всё чотко.  Зум просто великолепный, можно подглядовать в окна соседнего дома (100м)</p>', '<p>Общие характеристики.  Тип видеокамеры: HDD, цифровая  Тип носителя: жесткий диск  Емкость жесткого диска: 60 Гб  Режим ночной съемки: есть  Широкоформатный режим видео: есть    Объектив.  Фокусное расстояние объектива: 1.8 - 108 мм  Zoom оптический  цифровой: 60x  2000x  Выдержка: 13500 - 13 сек  Диафрагма: F1.8 - F6  Диаметр фильтра: 30 мм  Ручная фокусировка: есть    Фоторежим.  Фоторежим: есть  Число мегапикселов при фотосъемке: 0.41 Мпикс  Максимальное разрешение фотосъемки: 640x480 пикс    Дополнительная информация.  Минимальная освещенность: 6 люкс</p>', 'Ключевые слова', 'Описание', 12700, 0, 1, '2010-01-10', 1, 1, 0, 'bytovaja-tehnika/audio-i-video/video-kamery/sony-dcr-sr47e', 6, 7, '', '', 0, '2012-05-15', 0, 'com_inshop_item.tpl', '', 5, 1),
(3, 17, 3, '3001', 'Плеер MP3 Flash 2 GB teXet T-368 (2Gb) Black', '<p>Окунитесь в мир музыки с MP3-плеером<strong> teXet T-368 (2Gb)Bl</strong>. У этой модели такие компактные размеры - 88 мм в&nbsp; длину и маленький вес - всего лишь 30 грамм, что вы легко сможете носить его в кармане рубашки.</p>', '<p>Окунитесь в мир музыки с MP3-плеером<strong> teXet T-368 (2Gb)Bl</strong>. У этой модели такие компактные размеры - 88 мм в&nbsp; длину и маленький вес - всего лишь 30 грамм, что вы легко сможете носить его в кармане рубашки. Он без конвертации воспроизведет вашу любимую музыку, записанную в форматах MP3, WMA или WAV. 2 Гб встроенной памяти хватит для записи на плеер до 500 песен. К вашим услугам FM-тюнер с поддержкой текстовых сообщений, встроенные диктофон и микрофон, с которыми вы сможете записать заметки или понравившуюся композицию. На 0.98-дюймовом цветном дисплее вы в любой момент сможете прочитать информацию о песне и альбоме, которые вы сейчас слушаете. С помощью русифицированного меню вы без труда разберетесь в медиаархивах, хранящихся в памяти плеера. А одной пальчиковой батарейки вам хватит на 12 часов непрерывной работы миниатюрного teXet T-368.</p>', '', '', 1200, 0, 1, '2010-01-11', 0, 1, 0, 'bytovaja-tehnika/audio-i-video/mp3-plery/pler-mp3-flash-2-gb-texet-t-368-2gb-black', 8, 0, '', '', 0, '2012-05-16', 0, 'com_inshop_item.tpl', '', 4, 1),
(4, 17, 0, '3002', 'Плеер MP3 Flash 2 GB Rover Media C20R (2Gb) Black', '<p>Черно-оранжевый <strong>MP3-плеер RoverMediaC20R</strong> можно напрямую подключать к любым внешним источникам через высокоскоростной USB 2.0 коннектор.</p>', '<p>Встроенный FM-трансмиттер позволит слушать сохраненные в плеере музыкальные файлы в автомобиле, через домашнюю акустическую систему или музыкальный центр. Плеер поддерживает все необходимые аудиоформаты: MP3, WAV и WMA. Вы сможете записывать радиопередачи с FM-тюнера на встроенную память объемом 2 Гб. Плеер оборудован дополнительным слотом памяти micro SD, монохромным дисплеем с диагональю 1.2 дюйма, встроенным диктофоном с возможностью записи до 140 часов и эквалайзером, который поддерживает 6 фиксированных настроек.</p>', '', '', 1405, 4600, 1, '2010-01-11', 0, 1, 0, 'bytovaja-tehnika/audio-i-video/mp3-plery/pler-mp3-flash-2-gb-rover-media-c20r-2gb-black', 10, 0, '', '', 0, '2012-05-16', 0, 'com_inshop_item.tpl', '', 0, 0),
(5, 18, 4, '11203', 'Bosch WLX 20161', '', '', 'Bosch, стиральная машина, WLX 20161', 'WLX 20161', 15500, 0, 1, '2010-01-11', 0, 1, 0, 'bytovaja-tehnika/stiralnye-mashiny/bosch-wlx-20161', 3, 3, '', '', 0, '2012-03-02', 0, 'com_inshop_item.tpl', '', 0, 0),
(6, 18, 5, '1442', 'Indesit WISN 101', '<p><font class="t12">Материал бака пластик. Электронное управление. Индикация этапов программы. Распознавание пенообразования. Электронный контроль дисбаланса. Основные программы: ручная стирка шерсти. Специальные программы: стирка деликатных тканей, экспресс-стирка, программа стирки шерсти. Индикация этапов программы. Понижение скорости отжима.</font></p>', '<p><font class="t12">Материал бака пластик. Электронное управление. Индикация этапов программы. Распознавание пенообразования. Электронный контроль дисбаланса. Основные программы: ручная стирка шерсти. Специальные программы: стирка деликатных тканей, экспресс-стирка, программа стирки шерсти. Индикация этапов программы. Понижение скорости отжима.</font></p>', 'Indesit WISN 101, стиральная машина', 'Indesit WISN 101', 7050, 0, 1, '2010-01-11', 1, 1, 0, 'bytovaja-tehnika/stiralnye-mashiny/indesit-wisn-101', 0, 0, '', '', 0, '2011-05-18', 0, 'com_inshop_item.tpl', '', 0, 0),
(8, 22, 0, '101', 'InstantCMS 1.5.3 "Движок" для сайта', '', '', '', '', 1250, 0, 1, '2010-01-15', 0, 1, 1, 'programnoe-obespechenie/instantcms-1-5-3-dvizhok-dlja-saita', 11, 49, 'shop-20a9b2cb0f41.file', 'instantCMS_20091228_v1.5.3.zip', 3790433, '2010-01-17', 0, 'com_inshop_item.tpl', '', 0, 0),
(10, 23, 0, 'F001', 'Футболка мужская', '', '<p>Футболка мужская хлопковая</p>', '', '', 350, 0, 1, '2011-09-26', 0, 0, 0, 'futbolki/futbolka-muzhskaja', 1000, 0, '', '', 0, '2012-03-02', 0, 'com_inshop_item.tpl', '', 0, 0),
(11, 23, 0, 'F002', 'Футболка женская', '', '<p>Футболка женская, синтетическая</p>', '', '', 350, 0, 1, '2011-09-26', 0, 0, 0, 'futbolki/futbolka-zhenskaja', 97, 0, '', '', 0, '2012-03-02', 0, 'com_inshop_item.tpl', '', 0, 0);

DROP TABLE IF EXISTS `#__shop_items_bind`;

CREATE TABLE `#__shop_items_bind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `art_no` varchar(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_items_bind` (`id`, `item_id`, `art_no`, `title`, `price`, `qty`) VALUES
(28, 2, '0001w', 'белая', 12580, 6),
(27, 2, '0001g', 'серая', 12700, 0),
(31, 3, '3001a', 'Серебристый корпус', 1200, 3),
(23, 10, 'f002', 'красивая', 340, 1000),
(24, 10, 'f003', 'страшная', 400, 1000),
(32, 3, '3001b', 'Черный корпус', 1250, 5);

DROP TABLE IF EXISTS `#__shop_items_cats`;

CREATE TABLE `#__shop_items_cats` (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  KEY `item_id` (`item_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_items_cats` (`item_id`, `category_id`, `ordering`) VALUES
(6, 14, 5),
(2, 15, 2),
(5, 14, 4),
(4, 14, 3),
(4, 17, 2),
(5, 18, 1),
(2, 16, 1),
(2, 14, 1),
(4, 15, 3),
(6, 18, 2),
(3, 17, 1),
(3, 15, 1),
(3, 14, 2),
(8, 22, 1),
(10, 23, 1),
(11, 23, 2);

DROP TABLE IF EXISTS `#__shop_loads`;

CREATE TABLE `#__shop_loads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_key` varchar(32) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `filename_orig` varchar(250) NOT NULL,
  `is_loaded` tinyint(4) NOT NULL,
  `load_date` datetime NOT NULL,
  `load_ip` varchar(15) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_key` (`link_key`),
  KEY `order_id` (`order_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_orders`;

CREATE TABLE `#__shop_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secret_key` varchar(32) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_payment` datetime DEFAULT NULL,
  `date_closed` datetime DEFAULT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_org` varchar(150) NOT NULL,
  `customer_phone` varchar(30) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_address` varchar(250) NOT NULL,
  `customer_comment` varchar(250) NOT NULL,
  `customer_inn` varchar(32) NOT NULL,
  `items` text NOT NULL,
  `d_type` smallint(6) NOT NULL,
  `d_price` float NOT NULL,
  `giftcode` varchar(32) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `summ` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `psys_title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `secret_key` (`secret_key`,`user_id`),
  KEY `secret_key_2` (`secret_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_psys`;

CREATE TABLE `#__shop_psys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(24) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `config` text NOT NULL,
  `published` tinyint(4) NOT NULL,
  `ordering` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link` (`link`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_psys` (`id`, `link`, `title`, `url`, `logo`, `config`, `published`, `ordering`) VALUES
(1, 'cash', 'Оплата наличными', '', 'logo.gif', '---\ncurrency: \n  RUR: 1\n', 1, 1),
(5, 'webmoney', 'WebMoney Transfer', 'http://www.webmoney.ru/', 'logo.gif', '---\ncurrency: \n  WMR: 1\n  WMZ: 30\n  WME: 42\n  WMU: 0.9\n  WMB: 0.0054\nLMI_PAYEE_PURSE_R: \n  title: Кошелек продавца (WMR)\n  value: \nLMI_PAYEE_PURSE_Z: \n  title: Кошелек продавца (WMZ)\n  value: \nLMI_PAYEE_PURSE_E: \n  title: Кошелек продавца (WME)\n  value: \nLMI_PAYEE_PURSE_U: \n  title: Кошелек продавца (WMU)\n  value: \nSECRET_KEY: \n  title: Секретный ключ\n  value: \nLMI_SIM_MODE: \n  title: Режим тестирования (0,1,2)\n  value: 2\nPAYMENT_URL: \n  title: URL для отправки платежа\n  value: >\n    https://merchant.webmoney.ru/lmi/payment.asp\n', 1, 5),
(6, 'sberbank', 'СберБанк РФ', 'http://www.sbrf.ru/', 'logo.gif', '---\ncurrency: \n  RUR: 1\nSBRF_SHOP: \n  title: Наименование получателя платежа\n  value: \nSBRF_SHOP_INN: \n  title: ИНН получателя платежа\n  value: \nSBRF_SHOP_KPP: \n  title: КПП  получателя платежа\n  value: \nSBRF_SHOP_ACC: \n  title: Счет получателя платежа\n  value: \nSBRF_SHOP_BANK: \n  title: Наименование банка получателя\n  value: \nSBRF_SHOP_BIK: \n  title: БИК\n  value: \nSBRF_SHOP_KS: \n  title: Кор./сч.\n  value: \n', 1, 3),
(7, 'bill', 'Безналичный расчет', '', 'logo.gif', '---\ncurrency: \n  RUR: 1\nBILL_SHOP: \n  title: Наименование получателя платежа\n  value: \nBILL_SHOP_ADDR: \n  title: Адрес получателя платежа\n  value: \nBILL_SHOP_DIR: \n  title: Директор\n  value: \nBILL_SHOP_BUH: \n  title: Главный бухгалтер\n  value: \nBILL_SHOP_INN: \n  title: ИНН получателя платежа\n  value: \nBILL_SHOP_KPP: \n  title: КПП получателя платежа\n  value: \nBILL_SHOP_ACC: \n  title: Счет получателя платежа\n  value: \nBILL_SHOP_BANK: \n  title: Наименование банка получателя\n  value: \nBILL_SHOP_BIK: \n  title: БИК\n  value: \nBILL_SHOP_KS: \n  title: Кор./сч.\n  value: \nBILL_SHOP_NDS: \n  title: НДС, %\n  value: \n', 1, 4),
(8, 'robokassa', 'RoboKassa', 'http://www.robokassa.ru/', 'logo.gif', '---\ncurrency: \n  RUR: 1\nsMerchantLogin: \n  title: Логин продавца\n  value: \nsMerchantPass1: \n  title: |\n    Пароль #1\n  value: \nsMerchantPass2: \n  title: |\n    Пароль #2\n  value: \nsCulture: \n  title: Язык интерфейса РобоКассы (en/ru)\n  value: ru\nPAYMENT_URL: \n  title: URL для отправки платежа\n  value: http://test.robokassa.ru/Index.aspx\n', 1, 6),
(9, 'rbk', 'RBKmoney', 'http://www.rbkmoney.ru/', 'logo.gif', '---\ncurrency: \n  RUR: 1\nRBK_SHOP_ID: \n  title: ID сайта\n  value: \nRBK_SECRET_KEY: \n  title: Секретное слово\n  value: \nRBK_SUCCESS_URL: \n  title: Страница в случае успешного платежа\n  value: \nRBK_FAIL_URL: \n  title: Страница в случае не успешного платежа\n  value: \n', 1, 7),
(10, 'interkassa', 'Интеркасса', 'http://www.interkassa.com/', 'logo.gif', '---\ncurrency: \n  RUR: 1\nik_shop_id: \n  title: Идентификатор магазина\n  value: \nik_secret_key: \n  title: Секретный ключ\n  value: \nPAYMENT_URL: \n  title: URL для отправки платежа\n  value: >\n    http://www.interkassa.com/lib/payment.php\n', 1, 8),
(13, 'balance', 'Внутренний баланс аккаунта', '', 'logo.gif', '---\r\ncurrency: \r\n  RUR: 1\r\n', 1, 2);

DROP TABLE IF EXISTS `#__shop_ratings`;

CREATE TABLE `#__shop_ratings` (
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  KEY `item_id` (`item_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__shop_vendors`;

CREATE TABLE `#__shop_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `published` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `#__shop_vendors` (`id`, `title`, `published`) VALUES
(4, 'Bosch', 1),
(3, 'Sony', 1),
(5, 'Indesit', 1),
(6, 'Samsung', 1),
(7, 'Panasonic', 1),
(8, 'U-tel', 1);

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('maintop', 'InstantShop: Витрина', 'InstantShop: Витрина', '1', 'mod_inshop_front', '1', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('maintop', 'InstantShop: Новые товары', 'InstantShop: Новые товары', '1', 'mod_inshop_latest', '1', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('maintop', 'InstantShop: Популярные товары', 'InstantShop: Популярные товары', '1', 'mod_inshop_rating', '1', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`,`showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('sidebar', 'InstantShop: Корзина', 'InstantShop: Корзина', '1', 'mod_inshop_cart', '100', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('sidebar', 'InstantShop: Категории', 'InstantShop: Категории', '1', 'mod_inshop_tree', '1', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `cache`, `cachetime`, `cacheint`, `template`)
VALUES ('sidebar', 'InstantShop: Производители', 'InstantShop: Производители', '1', 'mod_inshop_vendors', '1', '1', '0', '0','', '1', '', '0', '1', 'HOUR', 'module.tpl');

INSERT INTO `#__modules` (`position`, `name`, `title`, `is_external`, `content`, `ordering`, `showtitle`, `published`, `user`, `config`, `original`, `css_prefix`, `access_list`, `cache`, `cachetime`, `cacheint`, `template`, `is_strict_bind`, `version`) VALUES
('sidebar', 'InstantShop: Фильтр', 'Фильтр товаров', 1, 'mod_inshop_filter', 2, 1, 1, 0, '---\n', 1, '', '', 0, 1, 'HOUR', 'module.tpl', 0, '1.0');

INSERT INTO `#__comment_targets` (`target`,`component`,`title`) VALUES ('shopitem', 'shop', 'Товары магазина');
