<?php

class p_maps_news extends cmsPlugin {

// ==================================================================== //

    public function __construct(){
        
        parent::__construct();

        // Информация о плагине

        $this->info['plugin']           = 'p_maps_news';
        $this->info['title']            = 'Новости пользователя на карте';
        $this->info['description']      = 'Добавляет вкладку "Новости" в профили всех пользователей';
        $this->info['author']           = 'InstantSoft';
        $this->info['version']          = '1.0';

        $this->info['tab']              = 'Новости'; //-- Заголовок закладки в профиле

        // События, которые будут отлавливаться плагином

        $this->events[]                 = 'USER_PROFILE';

    }

// ==================================================================== //

    /**
     * Процедура установки плагина
     * @return bool
     */
    public function install(){

        return parent::install();

    }

// ==================================================================== //

    /**
     * Процедура обновления плагина
     * @return bool
     */
    public function upgrade(){

        return parent::upgrade();

    }

// ==================================================================== //

    /**
     * Обработка событий
     * @param string $event
     * @param array $user
     * @return html
     */
    public function execute($event, $user){

        parent::execute();

        $inCore     = cmsCore::getInstance();

        if (!$inCore->isComponentInstalled('maps')){
            return 'Для работы плагина требуется компонент InstantMaps.';
        }

        $inCore->loadModel('maps');

        $model = new cms_model_maps();

        $model->where('o.user_id='.$user['id']);
        $model->orderBy('i.pubdate', 'DESC');
        $model->limit(20);
        $model->groupBy('i.id');

        $items      = $model->getNewsAll();

        $total      = sizeof($items);

        ob_start();

        $smarty= cmsPage::initTemplate('plugins', 'p_maps_news.tpl');
        $smarty->assign('total', $total);
        $smarty->assign('items', $items);
        $smarty->display('p_maps_news.tpl');

        $html = ob_get_clean();

        return $html;

    }

// ==================================================================== //

}

?>
