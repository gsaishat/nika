<?php

class p_maps_obj extends cmsPlugin {

// ==================================================================== //

    public function __construct(){
        
        parent::__construct();

        // Информация о плагине

        $this->info['plugin']           = 'p_maps_obj';
        $this->info['title']            = 'Объекты пользователя на карте';
        $this->info['description']      = 'Добавляет вкладку "Объекты" в профили всех пользователей';
        $this->info['author']           = 'InstantSoft';
        $this->info['version']          = '1.8';

        $this->info['tab']              = 'Объекты'; //-- Заголовок закладки в профиле

        // События, которые будут отлавливаться плагином

        $this->events[]                 = 'USER_PROFILE';
        $this->events[]                 = 'MAPS_LOAD_PROFILE_OBJECTS';

    }

// ==================================================================== //

    /**
     * Процедура установки плагина
     * @return bool
     */
    public function install(){

        return parent::install();

    }

// ==================================================================== //

    /**
     * Процедура обновления плагина
     * @return bool
     */
    public function upgrade(){

        return parent::upgrade();

    }

// ==================================================================== //

    /**
     * Обработка событий
     * @param string $event
     * @param array $user
     * @return html
     */
    public function execute($event, $user){

        parent::execute();
        
        $inCore     = cmsCore::getInstance();
        $inUser     = cmsUser::getInstance();

        if (!$inCore->isComponentInstalled('maps')){
            return 'Для работы плагина требуется компонент InstantMaps.';
        }

        $inCore->loadModel('maps');

        $model = new cms_model_maps();

        $page = isset($user['page']) ? $user['page'] : 1;
        $perpage = 10;

        $model->where('i.user_id='.$user['id']);
        $model->limitPage($page, $perpage);
        $model->groupBy('i.id');

        $total      = $model->getItemsCount();
        $items      = $model->getItems();

        if ($total > $perpage){
            $pages_url = 'javascript:getObjects(%page%)';
            $pagebar = cmsPage::getPagebar($total, $page, $perpage, $pages_url);
        } else {
            $pagebar = '';
        }
        
        ob_start();

        $smarty= cmsPage::initTemplate('plugins', 'p_maps_obj.tpl');
        $smarty->assign('total', $total);
        $smarty->assign('items', $items);
        $smarty->assign('pagebar', $pagebar);
        $smarty->assign('user_id', $user['id']);
        $smarty->assign('my_profile', $user['id']==$inUser->id);
        $smarty->display('p_maps_obj.tpl');

        $html = ob_get_clean();

        return $html;

    }

// ==================================================================== //

}

?>
