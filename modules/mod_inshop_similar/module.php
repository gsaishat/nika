<?php
function mod_inshop_similar($module, $cfg){
    $inCore     = cmsCore::getInstance();
    $inDb       = cmsDatabase::getInstance();

    $seolink    = $inCore->request('seolink', 'str', '');
    $do         = $inCore->request('do', 'str', 'view');

    if ($inCore->component != 'shop' || $do != 'item' || empty($seolink)) {
        return false;
    }

    global $_LANG;

    $inCore->loadLanguage('components/shop');

    $inCore->loadModel('shop');
    $model = new cms_model_shop();
    $item = $inDb->get_fields('cms_shop_items', "seolink = '{$seolink}'", 'id, category_id');

    $shop_cfg   = $inCore->loadComponentConfig('shop');

    if (!isset($cfg['orderby'])) { $cfg['orderby'] = 'id'; }
    if (!isset($cfg['orderto'])) { $cfg['orderto'] = 'desc'; }
    if (!isset($cfg['show_hit_img'])) { $cfg['show_hit_img'] = 1; }
    if (!isset($cfg['show_title'])) { $cfg['show_title'] = 0; }
    if (!isset($cfg['limit'])) { $cfg['limit'] = 12; }
    if (!isset($cfg['cols'])) { $cfg['cols'] = 4; }

    if ($item) {
        $model->where("i.id != {$item['id']}");
        $model->whereRecursiveCatIs($item['category_id']);
    }

    $model->groupBy('i.id');

    if ($cfg['orderby'] == 'rand') {
        $model->orderBy('RAND()');
    } else {
        $model->orderBy($cfg['orderby'], $cfg['orderto']);
    }

    $model->limitIs($cfg['limit']);

    $items = $model->getItems();

    if (empty($items)) {
        return false;
    }

    $smarty = cmsPage::initTemplate('modules', 'mod_inshop_similar.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('shop_cfg', $shop_cfg);
    $smarty->assign('items', $items);
    $smarty->assign('items_count', sizeof($items));
    $smarty->display('mod_inshop_similar.tpl');

    return true;

}
?>
