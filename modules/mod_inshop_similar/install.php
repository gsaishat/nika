<?php

// ========================================================================== //

    function info_module_mod_inshop_similar(){

        //
        // Описание модуля
        //

        //Заголовок (на сайте)
        $_module['title']        = 'InstantShop: Похожие товары';

        //Название (в админке)
        $_module['name']         = 'Похожие товары';

        //описание
        $_module['description']  = 'Показывает похожие товары InstantShop';
        
        //ссылка (идентификатор)
        $_module['link']         = 'mod_inshop_similar';
        
        //позиция
        $_module['position']     = 'sidebar';

        //автор
        $_module['author']       = 'IceJOKER';

        //текущая версия
        $_module['version']      = '1.0';

        //
        // Настройки по-умолчанию
        //
        $_module['config'] = array();

        return $_module;

    }

// ========================================================================== //

    function install_module_mod_inshop_similar(){

        return true;

    }

// ========================================================================== //

    function upgrade_module_mod_inshop_similar(){

        return true;
        
    }

// ========================================================================== //

?>