<?php
/*********************************************************************************************/
//																							 //
//                                InstantMaps v1.3   (c) 2010                                //
//	 					  http://www.instantcms.ru/, r2@instantsoft.ru                       //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2009-2010                        //
//                                                                                           //
/*********************************************************************************************/

function mod_maps_latest($module_id){

    $inCore     = cmsCore::getInstance();
    $inDB       = cmsDatabase::getInstance();
    $inPage     = cmsPage::getInstance();

    global $_LANG;

    $inCore->loadLanguage('components/maps');

    $inCore->loadModel('maps');
    $model = new cms_model_maps();

    $cfg = $inCore->loadModuleConfig($module_id);

	if (!isset($cfg['limit'])) { $cfg['limit'] = 10; }
	if (!isset($cfg['cat_id'])) { $cfg['cat_id'] = 0; }
	if (!isset($cfg['showcity'])) { $cfg['showcity'] = 'user'; }

    $city = $maps_cfg['city'];

    if ($cfg['cat_id']){
        $model->whereRecursiveCatIs($cfg['cat_id']);
    }

    if ($cfg['showcity'] == 'user'){
        $location = $model->detectUserLocation();
        if ($location['city']){ $model->whereCityMaybeIs($location['city']); }
    }

    if ($cfg['showcity'] == 'default'){
        $maps_cfg = $inCore->loadComponentConfig('maps');
        $model->whereCityIs($maps_cfg['city']);
    }

    $model->groupBy('i.id');
    $model->limit($cfg['limit']);
    $model->orderBy('i.pubdate', 'desc');

    $items = $model->getItems();

    $smarty = cmsPage::initTemplate('modules', 'mod_maps_latest.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('items', $items);
    $smarty->assign('items_count', sizeof($items));
    $smarty->display('mod_maps_latest.tpl');

    return true;

}
?>
