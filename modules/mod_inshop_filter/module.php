<?php
/*********************************************************************************************/
//																							 //
//                              InstantCMS v1.5   (c) 2009 FREEWARE                          //
//	 					  http://www.instantcms.ru/, info@instantcms.ru                      //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2007-2009                        //
//                                                                                           //
/*********************************************************************************************/

function mod_inshop_filter()
{
    $inCore = cmsCore::getInstance();
    $inDb   = cmsDatabase::getInstance();

    $inCore->loadModel('shop');
    $model = new cms_model_shop();

    $cfg = $inCore->loadComponentConfig('shop');

    $root_cat = array();
    if ($inCore->component == 'shop' && $_REQUEST['do'] == 'view' && $seolink = $inCore->request('seolink', 'str')) {
        $root_cat = $model->getCategoryByLink($seolink);
    }
    if (!$root_cat) {
        $root_cat = $model->getRootCategory();
    }

// ------- получаем значения фильтров -----------------

    //получаем производителей для фильтра
    $vendors = $model->getCatVendors($root_cat['id']);

    //получаем хар-ки категории
    $chars = $model->getCatChars($root_cat['id']);

    $filter = array();
    $filter_str = $_SESSION['shop_filters'][$root_cat['id']];

    if ($filter_str) {
        $filter = $model->parseFilterString($filter_str);
    }
    if ($inCore->inRequest('filter')) {
        $filter = $inCore->request('filter', 'array');
    }

    $max_price = $inDb->get_field('cms_shop_items', '1=1', 'MAX(price)');
    $price_to = $max_price > 100000 ? 100000 : 100000; //шаг по ценам в фильтре (2й элемент - шаг)
    $prices = range(0, $max_price, $price_to);

    $smarty = cmsPage::initTemplate('modules', 'mod_inshop_filter.tpl');
    $smarty->assign('filter', $filter);
    $smarty->assign('chars', $chars);
    $smarty->assign('vendors', $vendors);
    $smarty->assign('root_cat', $root_cat);
    $smarty->assign('max_price', $max_price);
    $smarty->assign('prices', $prices);
    $smarty->assign('cfg', $cfg);
    $smarty->display('mod_inshop_filter.tpl');

    return true;

}

?>