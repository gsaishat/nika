<?php

// ========================================================================== //

    function info_module_mod_inshop_latest_sidebar(){

        //
        // Описание модуля
        //

        //Заголовок (на сайте)
        $_module['title']        = 'InstantShop: Новые товары поселки';

        //Название (в админке)
        $_module['name']         = 'InstantShop: Новые товары поселки';

        //описание
        $_module['description']  = 'Показывает новые товары InstantShop';
        
        //ссылка (идентификатор)
        $_module['link']         = 'mod_inshop_latest_sidebar';
        
        //позиция
        $_module['position']     = 'maintop';

        //автор
        $_module['author']       = 'InstantSoft';

        //текущая версия
        $_module['version']      = '1.0';

        //
        // Настройки по-умолчанию
        //
        $_module['config'] = array();

        return $_module;

    }

// ========================================================================== //

    function install_module_mod_inshop_latest_sidebar(){

        return true;

    }

// ========================================================================== //

    function upgrade_module_mod_inshop_latest_sidebar(){

        return true;
        
    }

// ========================================================================== //

?>