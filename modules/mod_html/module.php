<?php

function mod_html(array $module, $cfg){
    if (!empty($cfg['url_pattern'])) {
        $pattern = $cfg['url_pattern'];
        if (!preg_match($pattern, $_SERVER['REQUEST_URI'])) {
            return false;
        }
    }

    $content = isset($cfg['content']) ? $cfg['content'] : '';

    $smarty = cmsPage::initTemplate('modules', 'mod_html.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('content', $content);
    $smarty->display('mod_html.tpl');

    return true;
}