<?php

function info_module_mod_html(){
    //Заголовок (на сайте)
    $_module['title']        = 'HTML текст';

    //Название (в админке)
    $_module['name']         = 'HTML текст';

    //описание
    $_module['description']  = 'Модуль для вывода HTML текста';

    //ссылка (идентификатор)
    $_module['link']         = 'mod_html';

    //позиция
    $_module['position']     = 'sidebar';

    //автор
    $_module['author']       = 'IceJOKER';

    //текущая версия
    $_module['version']      = '1.0';

    // Настройки по-умолчанию
    $_module['config'] = array();
    $_module['config']['url_pattern'] = '';
    $_module['config']['content'] = '';

    return $_module;
}
function install_module_mod_html(){

    return true;

}

function upgrade_module_mod_html(){

    return true;

}