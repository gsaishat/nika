<?php
/*********************************************************************************************/
//																							 //
//                                InstantMaps v1.3   (c) 2010                                //
//	 					  http://www.instantcms.ru/, r2@instantsoft.ru                       //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2009-2010                        //
//                                                                                           //
/*********************************************************************************************/

if (!function_exists('buildRating')){
	function buildRating($rating){
		$rating = round($rating, 2);
		$html = '';
		for($r = 0; $r < 5; $r++){
			if (round($rating) > $r){
				$html .= '<img src="/images/ratings/starfull.gif" border="0" title="'.$rating.'" />';
			} else {
				$html .= '<img src="/images/ratings/starhalf.gif" border="0" title="'.$rating.'" />';
			}
		}
		return $html;
	}
}

function mod_maps_rating($module_id){

    $inCore     = cmsCore::getInstance();
    $inDB       = cmsDatabase::getInstance();
    $inPage     = cmsPage::getInstance();

    global $_LANG;

    $inCore->loadLanguage('components/maps');

    $inCore->loadModel('maps');
    $model = new cms_model_maps();

    $cfg = $inCore->loadModuleConfig($module_id);

	if (!isset($cfg['limit'])) { $cfg['limit'] = 10; }
	if (!isset($cfg['cat_id'])) { $cfg['cat_id'] = 0; }
    if (!isset($cfg['showcity'])) { $cfg['showcity'] = 'user'; }

    if ($cfg['cat_id']){
        $model->whereCatIs($cfg['cat_id']);
    }
    
    if ($cfg['showcity'] == 'user'){
        $location = $model->detectUserLocation();
        if ($location['city']){ $model->whereCityMaybeIs($location['city']); }
    }

    if ($cfg['showcity'] == 'default'){
        $maps_cfg = $inCore->loadComponentConfig('maps');
        $model->whereCityIs($maps_cfg['city']);
    }

    $model->groupBy('i.id');
    $model->limit($cfg['limit']);
    $model->orderBy('i.rating', 'desc');

    $items = $model->getItems();

    foreach($items as $id=>$item){
        $items[$id]['rating_img'] = buildRating($item['rating']);
    }

    $smarty = cmsPage::initTemplate('modules', 'mod_maps_rating.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('items', $items);
    $smarty->assign('items_count', sizeof($items));
    $smarty->display('mod_maps_rating.tpl');

    return true;

}
?>
