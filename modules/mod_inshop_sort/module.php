<?php

function mod_inshop_sort(array $module, $cfg){
    global $_LANG;

    $inCore = cmsCore::getInstance();
    $inDb = cmsDatabase::getInstance();
    $inCore->loadLanguage('components/shop');

    //устанавливаем сортировку
    $orderby = $_SESSION['inshop_orderby'] ? $_SESSION['inshop_orderby'] : $cfg['items_orderby'];
    $orderto = $_SESSION['inshop_orderto'] ? $_SESSION['inshop_orderto'] : $cfg['items_orderto'];

    $order_types = array();

    foreach (array('ordering', 'price', 'title', 'id') as $order){

        switch ($order){
            case 'ordering': $name = $_LANG['SHOP_SORT_ORDERING']; break;
            case 'price': $name = $_LANG['SHOP_SORT_PRICE']; break;
            case 'title': $name = $_LANG['SHOP_SORT_TITLE']; break;
            case 'id': $name = $_LANG['SHOP_SORT_DATE']; break;
        }

        $order_types[] = array(
            'order' => $order,
            'name' => $name,
            'selected' => ($orderby==$order)
        );

    }

    $values = $inDb->get_field('cms_shop_chars', "id = 23", '`values`');
    $house_types = explode("\n", $values);
    $house_types = array_map('trim', $house_types);

    $values = $inDb->get_field('cms_shop_chars', "id = 33", '`values`');
    $districts = explode("\n", $values);
    $districts = array_map('trim', $districts);

    $values = $inDb->get_field('cms_shop_chars', "id = 12", '`values`');
    $areas = explode("\n", $values);
    $areas = array_map('trim', $areas);

    $smarty = cmsPage::initTemplate('modules', 'mod_inshop_sort.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('house_types', $house_types);
    $smarty->assign('districts', $districts);
    $smarty->assign('areas', $areas);
    $smarty->assign('order_types', $order_types);
    $smarty->assign('orderby', $orderby);
    $smarty->assign('orderto', $orderto);
    $smarty->display('mod_inshop_sort.tpl');
    return true;
}