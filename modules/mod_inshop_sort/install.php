<?php

function info_module_mod_inshop_sort(){
    //Заголовок (на сайте)
    $_module['title']        = 'Сортировка';

    //Название (в админке)
    $_module['name']         = 'Сортировка товаров';

    //описание
    $_module['description']  = 'Модуль для вывода блока сортировки товаров';

    //ссылка (идентификатор)
    $_module['link']         = 'mod_inshop_sort';

    //позиция
    $_module['position']     = 'sidebar';

    //автор
    $_module['author']       = 'IceJOKER';

    //текущая версия
    $_module['version']      = '1.0';

    // Настройки по-умолчанию
    $_module['config'] = array();

    return $_module;
}
function install_module_mod_inshop_sort(){

    return true;

}

function upgrade_module_mod_inshop_sort(){

    return true;

}