<?php
/*********************************************************************************************/
//																							 //
//                                InstantMaps v1.3   (c) 2010                                //
//	 					  http://www.instantcms.ru/, r2@instantsoft.ru                       //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2009-2010                        //
//                                                                                           //
/*********************************************************************************************/

function mod_maps_news($module_id){

    $inCore     = cmsCore::getInstance();
    $inDB       = cmsDatabase::getInstance();
    $inPage     = cmsPage::getInstance();

    global $_LANG;

    $inCore->loadLanguage('components/maps');

    $inCore->loadModel('maps');
    $model = new cms_model_maps();

    $maps_cfg   = $inCore->loadComponentConfig('maps');
    $cfg        = $inCore->loadModuleConfig($module_id);

	if (!isset($cfg['limit'])) { $cfg['limit'] = 10; }
	if (!isset($cfg['cat_id'])) { $cfg['cat_id'] = 0; }
	if (!isset($cfg['show_date'])) { $cfg['show_date'] = 1; }
	if (!isset($cfg['show_city'])) { $cfg['show_city'] = 1; }
	if (!isset($cfg['show_object'])) { $cfg['show_object'] = 1; }
    if (!isset($cfg['showcity'])) { $cfg['showcity'] = 'user'; }

    if ($cfg['cat_id']){
        $model->whereNewsCatIs($cfg['cat_id']);
    }

    if ($cfg['showcity'] == 'user'){
        $location = $model->detectUserLocation();
        if ($location['city']){ $model->whereNewsCityMaybeIs($location['city']); }
    }

    if ($cfg['showcity'] == 'default'){
        $maps_cfg = $inCore->loadComponentConfig('maps');
        $model->whereNewsCityIs($maps_cfg['city']);
    }

    $model->orderBy('i.pubdate', 'DESC');
    $model->groupBy('i.id');

    $model->limitIs($cfg['limit']);

    $items = $model->getNewsAll();

    $smarty = cmsPage::initTemplate('modules', 'mod_maps_news.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('maps_cfg', $maps_cfg);
    $smarty->assign('items', $items);
    $smarty->assign('items_count', sizeof($items));
    $smarty->display('mod_maps_news.tpl');

    return true;

}
?>
