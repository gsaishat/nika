<?php
/*********************************************************************************************/
//																							 //
//                                InstantMaps v1.3   (c) 2010                                //
//	 					  http://www.instantcms.ru/, r2@instantsoft.ru                       //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2009-2010                        //
//                                                                                           //
/*********************************************************************************************/

function mod_maps_front($module_id){

    $inCore     = cmsCore::getInstance();
    $inDB       = cmsDatabase::getInstance();
    $inPage     = cmsPage::getInstance();

    global $_LANG;

    $inCore->loadLanguage('components/maps');

    $inCore->loadModel('maps');
    $model = new cms_model_maps();

    $cfg        = $inCore->loadModuleConfig($module_id);
    $maps_cfg   = $inCore->loadComponentConfig('maps');

    if (!isset($cfg['autohide'])) { $cfg['autohide'] = 0; }
    if (!isset($cfg['show_title'])) { $cfg['show_title'] = 0; }
    if (!isset($cfg['cols'])) { $cfg['cols'] = 4; }
    if (!isset($cfg['cat_id'])) { $cfg['cat_id'] = 0; }
    if (!isset($cfg['showcity'])) { $cfg['showcity'] = 'user'; }

    if ($cfg['showcity']=='user'){
        $location = $model->detectUserLocation();
        $model->whereCityIs($location['city']);
    }

    $items = $model->getFrontItems($cfg['cat_id']);

    if (!$items && $cfg['autohide']) { return false; }

    $smarty = cmsPage::initTemplate('modules', 'mod_maps_front.tpl');
    $smarty->assign('cfg', $cfg);
    $smarty->assign('maps_cfg', $maps_cfg);
    $smarty->assign('items', $items);
    $smarty->assign('items_count', sizeof($items));
    $smarty->display('mod_maps_front.tpl');

    return true;

}
?>
