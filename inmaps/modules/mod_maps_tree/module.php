<?php
/*********************************************************************************************/
//																							 //
//                              InstantMaps v1.3  (c) 2009 FREEWARE                          //
//	 					  http://www.instantcms.ru/, info@instantcms.ru                      //
//                                                                                           //
// 						    written by Vladimir E. Obukhov, 2007-2009                        //
//                                                                                           //
/*********************************************************************************************/
	function mod_maps_tree($module_id){

        $inCore     = cmsCore::getInstance();
        $inDB       = cmsDatabase::getInstance();
		$cfg        = $inCore->loadModuleConfig($module_id);

        $inCore->loadModel('maps');
        $model = new cms_model_maps();

        if (!$cfg['parent_id']){ $cfg['parent_id'] = 0; }

        $items = $model->getCategories(true, $cfg['parent_id']);

        $current_id = 0;

        if ($_REQUEST['do']=='view'){
            $seolink     = $_REQUEST['seolink'];
            $current_cat = $model->getCategoryByLink($seolink);
            $current_id  = $current_cat['id'];
        }

        $smarty = cmsPage::initTemplate('modules', 'mod_maps_tree.tpl');
        $smarty->assign('items', $items);
        $smarty->assign('last_level', -1);
        $smarty->assign('hide_parent', 0);
        $smarty->assign('current_id', $current_id);
        $smarty->assign('cfg', $cfg);
        $smarty->display('mod_maps_tree.tpl');
	
		return true;
	
	}

?>