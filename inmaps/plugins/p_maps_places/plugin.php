<?php

class p_maps_places extends cmsPlugin {

// ==================================================================== //

    public function __construct(){
        
        parent::__construct();

        // Информация о плагине

        $this->info['plugin']           = 'p_maps_places';
        $this->info['title']            = 'Места, посещенные пользователем';
        $this->info['description']      = 'Добавляет вкладку "Места" в профили всех пользователей, с перечнем объектов для которых пользователь поставил отметку "Я здесь был"';
        $this->info['author']           = 'InstantSoft';
        $this->info['version']          = '1.9';

        $this->info['tab']              = 'Места'; //-- Заголовок закладки в профиле

        // События, которые будут отлавливаться плагином

        $this->events[]                 = 'USER_PROFILE';
        $this->events[]                 = 'MAPS_LOAD_PROFILE_PLACES';

    }

// ==================================================================== //

    /**
     * Процедура установки плагина
     * @return bool
     */
    public function install(){

        return parent::install();

    }

// ==================================================================== //

    /**
     * Процедура обновления плагина
     * @return bool
     */
    public function upgrade(){

        return parent::upgrade();

    }

// ==================================================================== //

    /**
     * Обработка событий
     * @param string $event
     * @param array $user
     * @return html
     */
    public function execute($event, $user){

        parent::execute();
        
        $inCore     = cmsCore::getInstance();
        $inUser     = cmsUser::getInstance();

        if (!$inCore->isComponentInstalled('maps')){
            return 'Для работы плагина требуется компонент InstantMaps.';
        }

        $inCore->loadModel('maps');

        $model = new cms_model_maps();

        $page = isset($user['page']) ? $user['page'] : 1;
        $perpage = 10;

        $model->limitPage($page, $perpage);

        $total      = $model->getAttendedItemsCount($user['id']);
        $items      = $model->getAttendedItems($user['id']);

        if ($total > $perpage){
            $pages_url = 'javascript:getPlaces(%page%)';
            $pagebar = cmsPage::getPagebar($total, $page, $perpage, $pages_url);
        } else {
            $pagebar = '';
        }

        $is_ajax = $event == 'MAPS_LOAD_PROFILE_PLACES';

        ob_start();

        $smarty= cmsPage::initTemplate('plugins', 'p_maps_places.tpl');
        $smarty->assign('is_ajax', $is_ajax);
        $smarty->assign('total', $total);
        $smarty->assign('items', $items);
        $smarty->assign('pagebar', $pagebar);
        $smarty->assign('user_id', $user['id']);
        $smarty->assign('my_profile', $user['id']==$inUser->id);
        $smarty->display('p_maps_places.tpl');

        $html = ob_get_clean();

        return $html;

    }

// ==================================================================== //

}

?>
