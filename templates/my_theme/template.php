<?php
/******************************************************************************/
//                                                                            //
//                           InstantCMS v1.10.7                               //
//                        http://www.instantcms.ru/                           //
//                                                                            //
//                   written by InstantCMS Team, 2007-2015                    //
//                produced by InstantSoft, (www.instantsoft.ru)               //
//                                                                            //
//                        LICENSED BY GNU/GPL v2                              //
//                                                                            //
/******************************************************************************/
//Файлы в папке modules/components твоей ТЕМЫ можно копировать из дефолтной по мере необходимости,
// сайт сначала попытается загрузить файл из текущей темы, если не нашел, то возьмет из дефолтной папки _default)
//кроме двух файлов в папке modules/module.tpl | module_simple.tpl - эти файлы используются, чтобы вывести данные всех модулей
//в админке можно выбрать в каком из файлов выводить данные
//в общем - вперед =)
/*сначала верстаешь, потом уже адаптируешь под движок, в начале я так делал    )
/templates/<?php echo TEMPLATE; ?>/images/*/

    /*
     * Доступны объекты $inCore $inUser $inPage($this) $inConf $inDB
     */

    // Получаем количество модулей на нужные позиции
    $mod_count['top']     = $this->countModules('top');
    $mod_count['topmenu'] = $this->countModules('topmenu');
    $mod_count['sidebar'] = $this->countModules('sidebar');

    // подключаем jQuery и js ядра в самое начало
    $this->prependHeadJS('core/js/common.js');
    $this->prependHeadJS('includes/jquery/jquery.js');
    // Подключаем стили шаблона
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/reset.css');
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/text.css');
    /*$this->addHeadCSS('templates/'.TEMPLATE.'/css/960.css');*/
    $this->addHeadCSS('templates/'.TEMPLATE.'/css/styles.css');
    // Подключаем colorbox (просмотр фото)
    $this->addHeadJS('includes/jquery/colorbox/jquery.colorbox.js');
    $this->addHeadCSS('includes/jquery/colorbox/colorbox.css');
    $this->addHeadJS('includes/jquery/colorbox/init_colorbox.js');
    // LANG фразы для colorbox
    $this->addHeadJsLang(array('CBOX_IMAGE','CBOX_FROM','CBOX_PREVIOUS','CBOX_NEXT','CBOX_CLOSE','CBOX_XHR_ERROR','CBOX_IMG_ERROR', 'CBOX_SLIDESHOWSTOP', 'CBOX_SLIDESHOWSTART'));
    $this->addHead('<script type="text/javascript">var TEMPLATE = "'.TEMPLATE.'";</script>');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns# video: http://ogp.me/ns/video# music: http://ogp.me/ns/music# ya: http://webmaster.yandex.ru/vocabularies/">

<head>
    <meta name=”viewport” content=”user-scalable=no” />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <!-- <meta name="viewport" content="width=device-width">-->
    <?php $this->printHead(); ?>
    <?php if($inUser->is_admin){ ?>
        <script src="/admin/js/modconfig.js" type="text/javascript"></script>
        <link href="/templates/<?php echo TEMPLATE; ?>/css/modconfig.css" rel="stylesheet" type="text/css" />
    <?php } ?>

    <link href="/templates/<?php echo TEMPLATE; ?>/css/style-slider.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/templates/<?php echo TEMPLATE; ?>/js/jquery.aw-showcase.js"></script>

    <script src="/templates/<?php echo TEMPLATE; ?>/js/jquery.fs.selecter.min.js"></script>
    <script src="/templates/<?php echo TEMPLATE; ?>/js/modernizr.js"></script>
    <link rel="stylesheet" href="/templates/<?php echo TEMPLATE; ?>/css/jquery.fs.selecter.css" />


<script type="text/javascript">
    window.onscroll = function() {
        var wrap = $("#wrap");
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled > 80) {
            wrap.addClass("fix-block");
        } else {
            wrap.removeClass("fix-block");
        }
    }
</script>
<style>
    @keyframes visibility {
        from {visibility: hidden;  opacity:0;}
        to {visibility: visible; opacity:1;}
    }
    .fix-block {
        position: fixed;
        top: 0;
        width: 100%;
        background: #ffffff;
        z-index: 999999;
        height:78px;
        min-width: 1200px;
        /*animation-name: visibility;
        animation-duration: 0.5s;*/
        animation: visibility 0.5s ease-in-out;
    }
</style>

</head>

<body>
<?php if ($inConf->siteoff && $inUser->is_admin) { ?>
<div style="margin:4px; padding:5px; border:solid 1px red; background:#FFF; position: fixed;opacity: 0.8; z-index:999"><?php echo $_LANG['SITE_IS_DISABLE']; ?></div>
<?php } ?>

<div id="wrap">
<div class="header-top" >
    <div class="content">
        <div class="logo">
            <a href="/"><img src="/templates/<?php echo TEMPLATE; ?>/images/logo.png"></a>
        </div>
        <div class="top-menu">
            <?php $this->printModules('header'); ?>
        </div>
        <div class="top-phone">
            <a href="tel:+79002290075">+7 900 22 900 75</a>
        </div>
        <div class="button">
            <a href="#modal" class="openModal">Заказать звонок</a>
        </div>
        <div id="modal" class="modal">
            <div>
                <p >Заказать звонок</p>
                <form id="backPhone" name="backPhone" action="/" method="post">
                    <script src="/templates/<?php echo TEMPLATE; ?>/js/jquery.maskedinput.min.js"></script>
                    <input name="fio" type="text" id="fio" maxlength="20" placeholder="Ф.И.О." required=”required” />
                    <input name="telephone" type="Tel" id="telForm" maxlength="20" placeholder="Телефон" required=”required” />
                      <script>
                        $(function(){
                            $("#telForm").mask("+7(999) 999-99-99");
                        });
                    </script>
                    <br/>
                    <button id="telButton" type="submit" form="backPhone">Отправить</button>
                </form>

                <a href="#close" title="Закрыть"></a>
            </div>
            <?php
            $fio = $_POST['fio'];
            $telephone = $_POST['telephone'];
            $fio = htmlspecialchars($fio);
            $telephone = htmlspecialchars($telephone);
            $fio = urldecode($fio);
            $telephone = urldecode($telephone);
            $fio = trim($fio);
            $telephone = trim($telephone);
            //echo $fio;
            //echo "<br>";
            //echo $telephone;
            if ((isset($fio)) && (isset($telephone))){
                mail("gadzhiibragimova@mail.ru", "Заявка с сайта", "ФИО:".$fio.". E-mail: ".$telephone ,"From: gadzhiibragimova@mail.ru \r\n");
                /*echo "сообщение успешно отправлено";*/
            } else {
                /*echo "при отправке сообщения возникли ошибки";*/
            }?>
        </div>
    </div>
</div>
</div>
<? if(!isset($_SERVER['REQUEST_URI']) OR $_SERVER['REQUEST_URI']=='/'){ ?>
<div id="front-img">
    <div style="background: rgba(255,255,255,0.5); width:100%; height: 100px;"></div>
    <div id="site-name">
        Дома от застройщика <br/> <span> в Краснодаре</span>
    </div>
    <div id="slogan">
        без посредников, без переплат
    </div>

</div>
<?}?>
<div id="filter">
    <div class="content">
        <?php $this->printModules('filter'); ?>
    </div>
</div>

<div class="content">
    <div class="conteiner">
        <div class="">
            <!--<div id="pathway">
               <?php /*$this->printPathway('&rarr;'); */?>
            </div>-->

            <div class="clear"></div>

            <?php $messages = cmsCore::getSessionMessages(); ?>
            <?php if ($messages) { ?>
                <div class="sess_messages" id="sess_messages">
                    <?php foreach($messages as $message){ ?>
                        <?php echo $message; ?>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if($this->page_body){ ?>
                <div class="component">
                    <?php $this->printBody(); ?>
                </div>
            <?php } ?>
        </div>


        <?php $this->printModules('content'); ?>


    </div>
    <div class="sidebar">
        <?php $this->printModules('sidebar'); ?>
    </div>
</div>

    <div class="footer">
        <div class="content">
            <div class="title">Контакты</div>
            <div class="left">
                <a href="tel:+79002290079"><span id="phone">+7 900 22 900 75</span></a>
                <a href="mailto:info.nika-dom@mail.ru"><span id="mail">info.nika-dom@mail.ru</span></a>
                <a href="/"><span id="adress">г. Краснодар ул. Стахановская 7</span></a>
            </div>
            <div class="center">
                <a href="https://yandex.ru/maps/?um=constructor%3Accb81b6c36dd894dae54fdd1a0e58fededd7eced3cfe149468280a2220a6ff25&amp;source=constructorStatic" target="_blank"><img src="https://api-maps.yandex.ru/services/constructor/1.0/static/?um=constructor%3Accb81b6c36dd894dae54fdd1a0e58fededd7eced3cfe149468280a2220a6ff25&amp;width=430&amp;height=210&amp;lang=ru_RU" alt="" style="border: 0;" /></a>

            </div>
            <div class="right-block">
                <div class="logo">
                    <a href="/"><img src="/templates/<?php echo TEMPLATE; ?>/images/logo-footer.png"></a>
                </div>
                <br/>
                <div class="copiright">
                    <p>НИКА-ДОМ © <?php echo date(Y);?></p>
                </div>

                <div class="sch">
                    <!--LiveInternet counter--><script type="text/javascript">
                        document.write("<a href='//www.liveinternet.ru/click' "+
                            "target=_blank><img src='//counter.yadro.ru/hit?t24.1;r"+
                            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                            ";"+Math.random()+
                            "' alt='' title='LiveInternet: показано число посетителей за"+
                            " сегодня' "+
                            "border='0' width='88' height='15'><\/a>")
                    </script><!--/LiveInternet-->

                </div>

                <div class="sitemap">
                    <a href="/sitemap"><img src="/templates/<?php echo TEMPLATE; ?>/images/sitemap.png"></a>
                </div>


            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(function(){
            $('#sess_messages').hide().fadeIn();
            $('#topmenu .menu li, #usermenu li').hover(
                function() {
                    $(this).find('ul:first').fadeIn('fast');
                    $(this).find('a:first').addClass("hover");
                },
                function() {
                    $(this).find('ul:first').hide();
                    $(this).find('a:first').removeClass("hover");
                }
            );
        });
    </script>
    <?php if($inConf->debug && $inUser->is_admin){ cmsPage::includeTemplateFile('special/debug.php'); } ?>
</body>
</html>