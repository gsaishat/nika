<div class="partn">
    {if $items}
    {* {$group.items|@count}
     {$items|@debug_print_var}*}

    {assign var="col" value="1"}
    {foreach key=tid item=item from=$items}
        {if $col==1}{/if}
        {*{$i|@debug_print_var}*}
        <div class="item">
            {*{$item.price|@debug_print_var}*}
            <div class="image">
                <a href="/shop/{$item.seolink}.html" title="{$item.title}">
                    <img src="/images/photos/small/{$item.filename}" border="0"/>
                    {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                </a>
            </div>
        </div>
        {*{$i|@debug_print_var}*}
        {if $col==$cfg.cols} {assign var="col" value="1"} {else} {math equation="x + 1" x=$col assign="col"} {/if}
    {/foreach}
    {if $col>1}
        <td colspan="{math equation="x - y + 1" x=$col y=$cfg.cols}">&nbsp;</td></tr>
    {/if}


</div>

{/if}