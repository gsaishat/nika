<div class="catalog" style="padding-top: 0;">
    <h1 style="margin-top: 0;">ПОХОЖИЕ</h1>
{if $items}
   {* {$group.items|@count}
    {$items|@debug_print_var}*}

    {assign var="col" value="1"}
    {foreach key=tid item=item from=$items}
        {if $col==1}{/if}
        {*{$i|@debug_print_var}*}
        <div class="item">
            {*{$item.price|@debug_print_var}*}
            <div class="image">
                <a href="/shop/{$item.seolink}.html" title="{$item.title}">
                    <img src="/images/photos/small/{$item.filename}" border="0"/>
                    {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                </a>
            </div>
            <div class="title">
                <a href="/shop/{$item.seolink}.html">{$item.title}</a>
            </div>
            <div class="price">
                {$item.price}, ₽
            </div>
            <div class="parameters">
                <p>Площадь дома: <span class="value">{$item.chars.20.value}м<sup>2</sup></span></p>
                <p>Тип дома: <span class="value">{$item.chars.20.value}</span></p>
                <p>Степень готовности: <span class="value">{$item.chars.18.value}</span></p>
            </div>
            <div class="button">
                <a href="/shop/{$item.seolink}.html">Подробнее</a>
            </div>

        </div>
        {*{$i|@debug_print_var}*}
        {if $col==$cfg.cols} {assign var="col" value="1"} {else} {math equation="x + 1" x=$col assign="col"} {/if}
    {/foreach}
    {if $col>1}
        <td colspan="{math equation="x - y + 1" x=$col y=$cfg.cols}">&nbsp;</td></tr>
    {/if}
</div>

{/if}