<div class="mod_shop_items_sort sort">
    {*<span>{$LANG.SHOP_SORT_BY}:</span>*}
    <div class="title">
        Коттеджные поселки
    </div>
   {* {foreach key=t item=type from=$order_types}
        {if $type.selected}
            <a href="/shop/sort/{$type.order}/{if $orderto=='asc'}desc{else}asc{/if}" class="selected">
                {$type.name}
                {if $orderto=='asc'}&darr;{else}&uarr;{/if}</a>
        {else}
            <a href="/shop/sort/{$type.order}/asc">{$type.name}</a>
        {/if}
        {if $t<3}|{/if}
    {/foreach}*}
    <div class="spisok">
        <ul class="list_img" id="ul">
            <li><a href="/shop/kottedzhnye-poselki?orderby=title?ordering&orderto=asc">КП по алфавиту</a></li>
            <li class="c1">КП по расположению</li>
                <ul id="c1" class="list_imgs">
                    <li><a>По округу</a></li>
                    <ul class="filter-kp">
                        {foreach item=district from=$districts}
                            <li> <a data-filter-id="33" href="/shop/kottedzhnye-poselki">{$district}</a></li>
                        {/foreach}
                    </ul>
                    <li><a>По району</a></li>
                    <ul class="filter-kp">
                        {foreach item=area from=$areas}
                            <li> <a data-filter-id="12" href="/shop/kottedzhnye-poselki">{$area}</a></li>
                        {/foreach}
                    </ul>
                </ul>
            <li class="c3">КП по типу недвижимости</li>
                <ul id="c3" class="list_imgs">
                    {foreach item=type from=$house_types}
                        <li><a data-filter-id="23" href="/shop/kottedzhnye-poselki">{$type}</a></li>
                    {/foreach}
                </ul>
        </ul>
    </div>
</div>

<script>
    window.onload = function () {
        document.getElementById("ul").onclick = function a(e) {
            e = e || event;
            var target = e.target || e.srcElement;
            if (target.tagName == "LI") {
                document.getElementById(target.className).style.display = document.getElementById(target.className).style.display == "block" ? "none" : "block";
                target.style.background = document.getElementById(target.className).style.display == "block" ? "#0ac504 url(/images/select-bg-hover.png) no-repeat 93% 23px" : "#fff url(/images/select-bg.png) no-repeat 93% 23px";
                target.style.color = document.getElementById(target.className).style.display == "block" ? "#ffffff" : "#313131";
            }

        }
    }

    $('a[data-filter-id]').click(function(e){
        e.preventDefault();
        var el = $(this);
        var filterId = el.data('filter-id');
        var url = el.attr('href');
        url += '?filter[' + filterId + ']=' + el.text();
        location.href = url;
    });
</script>