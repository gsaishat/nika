<div class="catalog">
{if $items}
   {* {$group.items|@count}
    {$items|@debug_print_var}*}

    {assign var="col" value="1"}
    {assign var="i" value="1"}
    {foreach key=tid item=item from=$items}
        {if $i==1}
            {if $col==1}{/if}
            <div class="item last">
                <div class="image">
                    <a href="/shop/{$item.seolink}.html" title="{$item.title}">
                        <img src="/images/photos/small/{$item.filename}" border="0"/>
                        {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                    </a>
                </div>
                <div class="title">
                    <a href="/shop/{$item.seolink}.html">{$item.title}</a>
                </div>

                <div class="left">
                    <div class="parameters">
                        <p>Площадь дома: <span class="value">{$item.chars.20.value} м<sup>2</sup></span></p>
                        <p>Тип дома: <span class="value">{$item.chars.20.value}</span></p>
                        <p>Степень готовности: <span class="value">{$item.chars.18.value}</span></p>
                    </div>
                </div>
                <div class="right">
                    <div class="price">
                        {$item.price}, ₽
                    </div>
                    <div class="button">
                        <a href="/shop/{$item.seolink}.html">Подробнее</a>
                    </div>
                </div>
            </div>
        {else}
            {if $col==1}{/if}
            {*{$i|@debug_print_var}*}
            <div class="item">
                {*{$item.price|@debug_print_var}*}
                <div class="image">
                    <a href="/shop/{$item.seolink}.html" title="{$item.title}">
                        <img src="/images/photos/small/{$item.filename}" border="0"/>
                        {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                    </a>
                </div>
                <div class="title">
                    <a href="/shop/{$item.seolink}.html">{$item.title}</a>
                </div>
                <div class="price">
                    {$item.price}, ₽
                </div>
                <div class="parameters">
                    <p>Площадь дома: <span class="value">{$item.chars.20.value}м<sup>2</sup></span></p>
                    <p>Тип дома: <span class="value">{$item.chars.20.value}</span></p>
                    <p>Степень готовности: <span class="value">{$item.chars.18.value}</span></p>
                </div>
                <div class="button">
                    <a href="/shop/{$item.seolink}.html">Подробнее</a>
                </div>

            </div>
        {/if}
        {math equation="x + 1" x=$i assign="i"}
        {*{$i|@debug_print_var}*}
        {if $col==$cfg.cols} {assign var="col" value="1"} {else} {math equation="x + 1" x=$col assign="col"} {/if}
    {/foreach}
    {if $col>1}
        <td colspan="{math equation="x - y + 1" x=$col y=$cfg.cols}">&nbsp;</td></tr>
    {/if}

    <div class="button all">
        <a href="/shop/doma">Смотреть все</a>
    </div>
</div>



  {*  {assign var="col" value="1"}
    {foreach key=tid item=item from=$items}
        {if $col==1} <tr> {/if}
        <td valign="top" width="{$colwidth}%">

            <div class="item_wrap">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td height="200">

                            {if $cfg.show_title}
                                <div class="title">
                                    <a href="/shop/{$item.seolink}.html">{$item.title}</a>
                                </div>
                            {/if}
                            <div class="image">
                                <a href="/shop/{$item.seolink}.html" title="{$item.title}">
                                    <img src="/images/photos/small/{$item.filename}" border="0"/>
                                    {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                                </a>
                            </div>
                        </td>
                    </tr>
                    {if $shop_cfg.is_shop && $item.price}
                        <tr>
                            <td height="40">
                                <div class="price"><span>{$item.price} {$shop_cfg.currency}</span></div>
                            </td>
                        </tr>
                    {/if}
                </table>
            </div>

        </td>
        {if $col==$cfg.cols} </tr> {assign var="col" value="1"} {else} {math equation="x + 1" x=$col assign="col"} {/if}
    {/foreach}
    {if $col>1}
        <td colspan="{math equation="x - y + 1" x=$col y=$cfg.cols}">&nbsp;</td></tr>
    {/if}*}

{/if}