<script>
    $(document).ready(function(){
        $("select").selecter();
    });
</script>

<script type="text/javascript">

</script>
<form action="/shop/doma" method="post">
    <div class="filter-item prices">
        <span class="title">Цена, ₽</span>
        <div class="filter-price">
            <div class="filter1">
                <select id="price1" name="filter[pfrom]" placeholder="От">
                    <option value="" selected="selected">От</option>
                    {foreach from=$prices item=price}
                        <option value="{$price}">{$price}, ₽</option>
                    {/foreach}
                </select>
            </div>
            <div class="filter2">
                <select id="price2" name="filter[pto]" placeholder="До" >
                    <option value="" selected="selected">До</option>
                    {foreach from=$prices item=price}
                        <option value="{$price}">{$price}, ₽</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>

    {if $cfg.show_filter_vendors && is_array($vendors)}
        <span class="title">{$LANG.SHOP_VENDORS}:</span>
        <div class="filter">
            {foreach key=vendor_id item=vendor from=$vendors}
                <div>
                    <label>
                        <input type="checkbox" value="{$vendor.id}" name="filter[vendors][]" {if in_array($vendor.id, $filter.vendors)}checked="checked"{/if} /> {$vendor.title}
                    </label>
                </div>
            {/foreach}
        </div>
    {/if}


    {foreach key=tid item=char from=$chars}
        {if $char.is_filter}
            <div class="filter-item">
                <span class="title">{$char.title}{if $char.units}, {$char.units}{/if}</span>
                <div class="filter">
                    {if $char.fieldtype != 'int'}
                        {if $char.values}
                            {if $char.is_filter_many}
                                {foreach key=vid item=val from=$char.values_arr}
                                    <div>
                                        <label><input type="checkbox" value="{$val}" name="filter[{$char.id}][]" {if in_array(trim($val), $filter[$char.id])}checked="checked"{/if} /> {$val}</label>
                                    </div>
                                {/foreach}
                            {else}
                                <select  name="filter[{$char.id}]">
                                    <option value="" {if !$filter[$char.id]}selected="selected"{/if}>Выбрать</option>
                                    {foreach key=vid item=val from=$char.values_arr}
                                        <option value="{$val}" {if trim($filter[$char.id]) == trim($val)}selected="selected"{/if}>{$val}</option>
                                    {/foreach}
                                </select>
                            {/if}
                        {else}
                            <input type="text" name="filter[{$char.id}]" class="input" value="{$filter[$char.id]}" style="width:99%"/>
                        {/if}
                    {else}
                        <input type="text" name="filter[{$char.id}][from]" class="input" value="{$filter[$char.id].from}" style="width:102px"/>
                        <input type="text" name="filter[{$char.id}][to]" class="input" value="{$filter[$char.id].to}" style="width:102px"/>
                    {/if}
                </div>
            </div>
        {/if}
    {/foreach}

    <a href="/shop/doma" class="clear-filter">
        <img class="icon-zoom" src="/templates/my_theme/images/clear.png" />
    </a>

    <div id="clear"></div>
    <div class="button">
        <input type="submit" value="Подобрать" />
          {*  {if $filter}<input type="button" value="{$LANG.SHOP_FILTER_CANCEL}" onclick="window.location.href='/shop/{$root_cat.seolink}/all'" />{/if}*}
    </div>
</form>
