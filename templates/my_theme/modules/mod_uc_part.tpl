{add_js file="templates/my_theme/js/jquery.flexisel.js"}
{add_css file="templates/my_theme/css/style-partner.css"}

<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo3").flexisel({
            visibleItems: 4,
            itemsToScroll: 1,
            autoPlay: {
                enable: true,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 4,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 4,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 4,
                    itemsToScroll: 3
                }
            }
        });
    });
</script>


<div class="partnery">
    <h1>Наши партнеры</h1>
    <ul id="flexiselDemo3">
        {if $cfg.showtype == 'thumb'}
        {foreach key=tid item=item from=$items}
            {if $item}
                <li>
                    <a class="mod_latest_title" href="/catalog/item{$item.id}.html"><img src="/images/catalog/{$item.imageurl}" alt="{$item.title|escape:'html'}"/></a>
                </li>
            {/if}
        {/foreach}
        {/if}
    </ul>
</div>
