{add_js file="templates/my_theme/js/jquery.flexisel.js"}
{add_css file="templates/my_theme/css/style-partner.css"}

<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo3").flexisel({
            visibleItems: 4,
            itemsToScroll: 1,
            autoPlay: {
                enable: true,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 4,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 4,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 4,
                    itemsToScroll: 3
                }
            }
        });
    });
</script>



{if $cfg.is_pag}
	<script type="text/javascript">
		function conPage(page, module_id){
            $('div#module_ajax_'+module_id).css({ opacity:0.4, filter:'alpha(opacity=40)' });
			$.post('/modules/mod_latest/ajax/latest.php', { 'module_id': module_id, 'page':page }, function(data){
				$('div#module_ajax_'+module_id).html(data);
                $('div#module_ajax_'+module_id).css({ opacity:1.0, filter:'alpha(opacity=100)' });
			});
		}
    </script>
{/if}
{if !$is_ajax}<div id="module_ajax_{$module_id}">{/if}

<div class="partnery">
	<h1>Наши партнеры</h1>
	<ul id="flexiselDemo3">
		{foreach key=aid item=article from=$articles}
			{if $article.image}
				<li>
					<a class="mod_latest_title" href="{$article.url}"><img src="/images/photos/medium/{$article.image}" alt="{$article.title|escape:'html'}"/></a>
				</li>
			{/if}
		{/foreach}
	</ul>
</div>

{if !$is_ajax}</div>{/if}