{if $items}
    {if $cfg.ratings}
        {add_js file='components/shop/js/rating/jquery.MetaData.js'}
        {add_js file='components/shop/js/rating/jquery.rating.js'}
        {add_css file='components/shop/js/rating/jquery.rating.css'}
    {/if}

    {add_js file="components/shop/js/cart.js"}

    {*<div class="shop_items_sort">
        <span>{$LANG.SHOP_SORT_BY}:</span>
        {foreach key=t item=type from=$order_types}
            {if $type.selected}
                <a href="/shop/sort/{$type.order}/{if $orderto=='asc'}desc{else}asc{/if}" class="selected">
                    {$type.name}
                    {if $orderto=='asc'}&darr;{else}&uarr;{/if}</a>
            {else}
                <a href="/shop/sort/{$type.order}/asc">{$type.name}</a>
            {/if}
            {if $t<3}|{/if}
        {/foreach}
    </div>*}


<div class="shop_items_list">
    <div class="catalog kp">
        {foreach key=num item=item from=$items}
            <div class="item">
                <div class="image">
                    <a href="/shop/{$item.seolink}.html">
                        <img src="/images/photos/small/{$item.filename}" border="0" />
                        {if $cfg.show_hit_img && $item.is_hit}<div class="is_hit"></div>{/if}
                    </a>
                    {if $cfg.ratings}
                        <div class="rating">
                            <form action="/shop/rate" method="POST">
                                <input type="hidden" name="item_id" value="{$item.id}" />
                                {section name=rate start=1 loop=6 step=1}
                                    <input name="rate" type="radio" class="star" value="{$smarty.section.rate.index}" {if $item.rating>=$smarty.section.rate.index}checked="checked"{/if} {if !$is_user || $item.user_voted}disabled="disabled"{/if} />
                                {/section}
                            </form>
                            {if $item.rating}
                                <small>{$item.rating}</small>
                            {/if}
                        </div>
                    {/if}
                </div>
                <div class="title">
                    <a href="/shop/{$item.seolink}.html">{$item.title}</a>
                    {if $cfg.show_vendors && $item.vendor}/ <a href="/shop/vendors/{$item.vendor_id}" class="vendor">{$item.vendor}</a>{/if}
                    {if $cfg.show_compare}
                        <span class="compare">
                            {if !$item.is_in_compare}
                                <a class="add" href="/shop/compare/{$item.id}">{$LANG.SHOP_COMPARE_ADD}</a>
                            {else}
                            {$LANG.SHOP_COMPARE_ITEM_IN} <a href="/shop/compare.html">{$LANG.SHOP_COMPARE_IN}</a>
                            {/if}
                        </span>
                    {/if}
                </div>
                <div class="parameters">
                    {foreach key=num item=char from=$item.chars}
                        {if $char.value}
                            {if ($char.title=="Цена от")}
                                {if !$char.is_custom}
                                    <p>{$char.title}: <span class="price">{"|"|str_replace:', ':$char.value} {if $char.units}{$char.units}{/if},Р</span></p>
                                {else}
                                    <p>{$char.title}:
                                    <span class="value">
                                        <select name="chars[{$char.id}]">
                                            {foreach key=c item=val from=$char.items}
                                                <option value="{$val}">{$val}</option>
                                            {/foreach}
                                        </select>
                                    </span></p>
                                {/if}
                            {elseif ($char.title=="Район")}
                                {if !$char.is_custom}
                                    <p>{$char.title}:
                                        <span class="value">{"|"|str_replace:', ':$char.value} {if $char.units}{$char.units}{/if}</span></p>
                                {else}
                                    <p>{$char.title}:
                                        <span class="value">
                                        <select name="chars[{$char.id}]">
                                            {foreach key=c item=val from=$char.items}
                                                <option value="{$val}">{$val}</option>
                                            {/foreach}
                                        </select>
                                    </span></p>
                                {/if}
                             {/if}
                        {/if}
                    {/foreach}
                </div>
                <div class="button">
                    <a href="/shop/{$item.seolink}.html">Подробнее</a>
                </div>
            </div>
        {/foreach}
    </div>
</div>

    <div class="clear"></div>

    {if $pages>1}
        <div class="shop_pages">
            {$pagebar}
        </div>
    {/if}

    {if $cfg.ratings}
        <script type="text/javascript">
        {literal}
            $('.star').rating({
                callback: function(value, link){
                    this.form.submit();
                }
            });
        {/literal}
        </script>
    {/if}

{/if}
