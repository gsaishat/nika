{$item.plugins_output_before}
<div class="main accii">
{if $cat.view_type=='shop' || $item.can_edit}
	<div id="shop_toollink_div">
    {if $cat.view_type=='shop'}
        {$shopCartLink}
    {/if}
    {if $item.can_edit}
        <a href="/catalog/edit{$item.id}.html" class="uc_item_edit_link">{$LANG.EDIT}</a>
    {/if}
    </div>
{/if}


<table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:10px; padding: 10px;"><tr>
	<td align="left" valign="top" width="10" class="uc_detailimg">

        <div id="photo-accii">
		{if strlen($item.imageurl)>4}
            <a class="lightbox-enabled" title="{$item.title|escape:'html'}" rel="lightbox" href="/images/catalog/{$item.imageurl}" target="_blank">
                <img alt="{$item.title|escape:'html'}" src="/images/catalog/medium/{$item.imageurl}" />
            </a>
        {else}
            <img src="/images/catalog/medium/nopic.jpg" border="0" />
        {/if}
        </div>

        {foreach key=field item=value from=$fields}
            {if $value}
                {if $field=="Местоположение"}
                    </ul><div class="place">{$value}</div>
                {/if}
            {/if}
        {/foreach}


    </td>

    <td class="uc_list_itemdesc" align="left" valign="top" class="uc_detaildesc">
        <h1 class="con_heading">{$item.title}</h1>
        <ul class="chars_list acciya">
			{foreach key=field item=value from=$fields}
                {if $value}
                    {if strstr($field, '/~l~/')}
                        <li class="uc_detailfield">{$value}</li>
                    {elseif $field=="Телефон"}
                        <li class="phone"><span class="quest">{$field}:</span><span class="answer">{$value}</span></li>
                    {elseif $field=="Описание"}
                        </ul><div class="description"><span class="quest">{$field}:</span><br><br>{$value}</div>
                    {elseif $field=="Местоположение"}
                        </ul>
                    {else}
                        <li><span class="quest">{$field}:</span><span class="answer">{$value}</span></li>
                    {/if}
                {/if}
			{/foreach}
		</ul>
        <br/>
        <a class="print" href="javascript:(print());"><span>Распечатать страницу</span></a>

        {if $cat.view_type=='shop'}
			<div id="shop_price">
                <span>{$LANG.PRICE}:</span> {$item.price} {$LANG.CURRENCY}
            </div>

			<div id="shop_ac_itemdiv">
                <a href="/catalog/addcart{$item.id}.html" title="{$LANG.ADD_TO_CART}" id="shop_ac_item_link">
					<img src="/components/catalog/images/shop/addcart.jpg" alt="{$LANG.ADD_TO_CART}"/>
                </a>
            </div>
        {/if}

        {if $item.on_moderate}

                <div id="shop_moder_form">
                    <p class="notice">{$LANG.WAIT_MODERATION}:</p>
                    <table cellpadding="0" cellspacing="0" border="0"><tr>
                    <td>
                            <form action="/catalog/moderation/accept{$item.id}.html" method="POST">
                                <input type="submit" name="accept" value="{$LANG.MODERATION_ACCEPT}"/>
                            </form>
                          </td>
                    <td>
                            <form action="/catalog/edit{$item.id}.html" method="POST">
                                <input type="submit" name="accept" value="{$LANG.EDIT}"/>
                            </form>
                          </td>
                    <td>
                            <form action="/catalog/moderation/reject{$item.id}.html" method="POST">
                                 <input type="submit" name="accept" value="{$LANG.MODERATION_REJECT}"/>
                            </form>
                          </td>
                    </tr></table>

                </div>
        {/if}

	</td>
</tr></table>

{if ($cat.showtags) && ($tagline)}
    <div class="uc_detailtags"><strong>{$LANG.TAGS}: </strong>{$tagline}</div>
{/if}

{if $cat.is_ratings}
	{$ratingForm}
{/if}

</div>
{$item.plugins_output_after}