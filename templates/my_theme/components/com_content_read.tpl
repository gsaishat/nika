{$article.plugins_output_before}
<div class="main" style="padding: 20px;">
{*{if $article.showtitle}*}
    <h1 class="con_heading">{$article.title}</h1>
{*{/if}*}
{*
{if $article.showdate}
	<div class="con_pubdate">
		{if !$article.published}<span style="color:#CC0000">{$LANG.NO_PUBLISHED}</span>{else}{$article.pubdate}{/if}
	</div>
{/if}*}

{if $is_pages}

	<div class="con_pt" id="pt">
		<span class="con_pt_heading">
			<a class="con_pt_hidelink" href="javascript:void;" onClick="$('#pt_list').toggle();">{$LANG.CONTENT}</a>
			{if $cfg.pt_hide} [<a href="javascript:void(0);" onclick="$('#pt').hide();">{$LANG.HIDE}</a>] {/if}
		</span>
		<div id="pt_list" style="{if $cfg.pt_disp}display: block;{else}display: none;{/if} width:100%">
			<div>
				<ul id="con_pt_list">
				{foreach key=tid item=pages from=$pt_pages}
					{if ($tid+1 != $page)}
						<li><a href="{$pages.url}">{$pages.title}</a></li>
					{else}
						<li>{$pages.title}</li>
					{/if}
				{/foreach}
				<ul>
			</div>
		</div>
	</div>

{/if}

<div class="con_text" style="overflow:hidden">

    {if $article.image}
        <div class="con_image" style="float:left;margin-top:10px;margin-right:20px;margin-bottom:20px">
            <img src="/images/photos/medium/{$article.image}" alt="{$article.title|escape:html}"/>
        </div>
    {/if}
    {$article.content}
</div>

</div>

