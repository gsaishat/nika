<div><ul id="inmaps_tree">

{foreach key=key item=item from=$items}

    {if $item.NSLevel < $last_level}
        {math equation="x - y" x=$last_level y=$item.NSLevel assign="tail"}
        {section name=foo start=0 loop=$tail step=1}
            </ul></li>
        {/section}
    {/if}
    {if $item.NSRight - $item.NSLeft == 1}
        <li>
            <span class="folder">
                {if $item.id != $current_id}
                    <a href="/maps/{$item.seolink}">{$item.title}</a>
                {else}
                    {$item.title}
                {/if}
            </span>
        </li>
    {else}
        <li {if $currentmenu.NSLeft > $item.NSLeft && $currentmenu.NSRight < $item.NSRight}class="open"{/if}">
            <span class="folder">
                {if $item.id != $current_id}
                    <a href="/maps/{$item.seolink}">{$item.title}</a>
                {else}
                    {$item.title}
                {/if}
            </span>
            <ul>
    {/if}
    {assign var="last_level" value=$item.NSLevel}

{/foreach}
</ul></div>
