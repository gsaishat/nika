<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-04-29 01:02:22
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\components\com_catalog_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:308245903bbee886494-17712309%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d319d7590ef264c2a92614fcb592618e4e87e59' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\components\\com_catalog_add.tpl',
      1 => 1470151098,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '308245903bbee886494-17712309',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'do' => 0,
    'LANG' => 0,
    'cat_id' => 0,
    'item' => 0,
    'is_admin' => 0,
    'cats' => 0,
    'cat' => 0,
    'fields' => 0,
    'field' => 0,
    'id' => 0,
    'cfg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5903bbeeb35e03_31405308',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5903bbeeb35e03_31405308')) {function content_5903bbeeb35e03_31405308($_smarty_tpl) {?><?php if (!is_callable('smarty_function_wysiwyg')) include 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\includes\\smarty\\libs\\plugins\\function.wysiwyg.php';
?><div class="con_heading">
    <?php if ($_smarty_tpl->tpl_vars['do']->value=='add_item') {
echo $_smarty_tpl->tpl_vars['LANG']->value['ADD_ITEM'];
}?>
    <?php if ($_smarty_tpl->tpl_vars['do']->value=='edit_item') {
echo $_smarty_tpl->tpl_vars['LANG']->value['EDIT_ITEM'];
}?>
</div>

<div id="configtabs">

    <div id="form">
        <form id="add_form" method="post" action="/catalog/<?php echo $_smarty_tpl->tpl_vars['cat_id']->value;?>
/submit.html" enctype="multipart/form-data">
        <table cellpadding="5" cellspacing="0" style="margin-bottom:10px" width="100%">
            <tr>
                <td width="210">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['TITLE'];?>
:</strong>
                </td>
                <td><input type="text" name="title" id="title" class="text-input" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" style="width:300px"/></td>
            </tr>
            <?php if ($_smarty_tpl->tpl_vars['is_admin']->value) {?>
            <tr>
                <td width="210">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['CAT'];?>
:</strong>
                </td>
                <td><select style="width:300px" class="text-input" name="new_cat_id" id="cat_id" ><?php echo $_smarty_tpl->tpl_vars['cats']->value;?>
</select></td>
            </tr>
            <?php }?>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['IMAGE'];?>
:</strong>
                </td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['do']->value=='edit_item'&&$_smarty_tpl->tpl_vars['item']->value['imageurl']) {?>
                        <div style="margin-bottom:4px;">
                            <a href="/images/catalog/<?php echo $_smarty_tpl->tpl_vars['item']->value['imageurl'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['item']->value['imageurl'];?>
</a>
                        </div>
                    <?php }?>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><input name="imgfile" type="file" id="imgfile" style="width:300px" class="text-input" /></td>
                            <?php if ($_smarty_tpl->tpl_vars['do']->value=='edit_item'&&$_smarty_tpl->tpl_vars['item']->value['imageurl']) {?>
                                <td style="padding-left:15px">
                                    <label>
                                        <input type="checkbox" value="1" name="delete_img" />
                                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['DELETE'];?>

                                    </label>
                                </td>
                            <?php }?>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php if ($_smarty_tpl->tpl_vars['cat']->value['view_type']=='shop') {?>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['PRICE'];?>
:</strong>
                </td>
                <td>
                    <input type="text" class="text-input" name="price" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['price'], ENT_QUOTES, 'UTF-8', true);?>
" style="width:300px"/>
                </td>
            </tr>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['CAN_MANY'];?>
:</strong>
                </td>
                <td>
                    <label><input type="radio" name="canmany" value="1" <?php if ($_smarty_tpl->tpl_vars['item']->value['canmany']) {?>checked="checked"<?php }?>> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['YES'];?>
 </label>
                    <label><input type="radio" name="canmany" value="0" <?php if (!$_smarty_tpl->tpl_vars['item']->value['canmany']) {?>checked="checked"<?php }?>> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['NO'];?>
 </label>
                </td>
            </tr>
            <?php }?>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['TAGS'];?>
:</strong><br/>
                    <span class="hint"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['KEYWORDS'];?>
</span>
                </td>
                <td>
                    <input type="text" name="tags" class="text-input" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['tags'], ENT_QUOTES, 'UTF-8', true);?>
" style="width:300px"/>
                </td>
            </tr>
        <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
            <tr>
                <?php if ($_smarty_tpl->tpl_vars['field']->value['ftype']=='link'||$_smarty_tpl->tpl_vars['field']->value['ftype']=='text') {?>
                <td valign="top">
                    <strong><?php echo $_smarty_tpl->tpl_vars['field']->value['title'];?>
:</strong>
                    <?php if ($_smarty_tpl->tpl_vars['field']->value['ftype']=='link') {?> <br/><span class="hint"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['TYPE_LINK'];?>
</span><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['field']->value['makelink']) {?> <br/><span class="hint"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['COMMA_SEPARATE'];?>
</span><?php }?>
                </td>
                <td>
                    <input style="width:300px" name="fdata[<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
]" type="text" class="text-input" value="<?php if ($_smarty_tpl->tpl_vars['field']->value['value']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['value'], ENT_QUOTES, 'UTF-8', true);
}?>"/>
                </td>
                <?php } else { ?>
                    <td valign="top"><strong><?php echo $_smarty_tpl->tpl_vars['field']->value['title'];?>
:</strong></td>
                    <td>
                        <?php echo smarty_function_wysiwyg(array('name'=>"fdata[".((string)$_smarty_tpl->tpl_vars['id']->value)."]",'value'=>$_smarty_tpl->tpl_vars['field']->value['value'],'height'=>300,'width'=>'98%'),$_smarty_tpl);?>

                    </td>
                <?php }?>
            </tr>
        <?php } ?>
        <?php if ($_smarty_tpl->tpl_vars['is_admin']->value) {?>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SEO_KEYWORDS'];?>
:</strong><br/>
                    <span class="hint"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SEO_KEYWORDS_HINT'];?>
</span>
                </td>
                <td>
                    <input type="text" name="meta_keys" class="text-input" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['meta_keys'], ENT_QUOTES, 'UTF-8', true);?>
" style="width:300px"/>
                </td>
            </tr>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SEO_DESCRIPTION'];?>
:</strong>
                </td>
                <td>
                    <input type="text" name="meta_desc" class="text-input" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['meta_desc'], ENT_QUOTES, 'UTF-8', true);?>
" style="width:300px"/>
                </td>
            </tr>
            <tr>
                <td width="">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['IS_COMMENTS'];?>
:</strong>
                </td>
                <td>
                    <label><input type="radio" name="is_comments" value="1" <?php if ($_smarty_tpl->tpl_vars['item']->value['is_comments']) {?>checked="checked"<?php }?>> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['YES'];?>
 </label>
                    <label><input type="radio" name="is_comments" value="0" <?php if (!$_smarty_tpl->tpl_vars['item']->value['is_comments']) {?>checked="checked"<?php }?>> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['NO'];?>
 </label>
                </td>
            </tr>
        <?php }?>
        </table>
        <?php if ($_smarty_tpl->tpl_vars['cfg']->value['premod']&&!$_smarty_tpl->tpl_vars['is_admin']->value) {?>
            <p style="margin-top:15px;color:red">
                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ITEM_PREMOD_NOTICE'];?>

            </p>
        <?php }?>
        <p style="margin-top:15px">
            <input type="hidden" name="opt" value="<?php if ($_smarty_tpl->tpl_vars['do']->value=='add_item') {?>add<?php } else { ?>edit<?php }?>" />
            <?php if ($_smarty_tpl->tpl_vars['do']->value=='edit_item') {?>
                <input type="hidden" id="item_id" name="item_id" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" />
            <?php }?>
            <input type="submit" name="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['SAVE'];?>
" style="font-size:18px" />
            <input type="button" name="back" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['CANCEL'];?>
"  style="font-size:18px" onClick="window.history.go(-1)" />
        </p>
        </form>
    </div>

</div>
<?php }} ?>
