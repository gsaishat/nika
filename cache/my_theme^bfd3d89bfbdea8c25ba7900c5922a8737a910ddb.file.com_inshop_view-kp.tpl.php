<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-04-30 21:54:16
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\components\com_inshop_view-kp.tpl" */ ?>
<?php /*%%SmartyHeaderCode:875590632d8e43491-22915926%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfd3d89bfbdea8c25ba7900c5922a8737a910ddb' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\components\\com_inshop_view-kp.tpl',
      1 => 1492974849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '875590632d8e43491-22915926',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'root_cat' => 0,
    'cfg' => 0,
    'subcats' => 0,
    'cat' => 0,
    'subcat' => 0,
    'num' => 0,
    'items' => 0,
    'filter' => 0,
    'LANG' => 0,
    'total' => 0,
    'vendors' => 0,
    'vendor' => 0,
    'chars' => 0,
    'char' => 0,
    'val' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_590632d9218290_40783545',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_590632d9218290_40783545')) {function content_590632d9218290_40783545($_smarty_tpl) {?>



<h1 class="con_heading"><?php echo $_smarty_tpl->tpl_vars['root_cat']->value['title'];?>
</h1>

<?php if ($_smarty_tpl->tpl_vars['root_cat']->value['description']) {?>
    <div style="margin-bottom:20px"><?php echo $_smarty_tpl->tpl_vars['root_cat']->value['description'];?>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cfg']->value['show_subcats']&&$_smarty_tpl->tpl_vars['subcats']->value) {?>
    <ul class="shop_cat_list">
        <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['tid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['subcats']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['tid']->value = $_smarty_tpl->tpl_vars['cat']->key;
?>
            <li class="shop_cat_item" style="background:url(/images/photos/small/<?php echo $_smarty_tpl->tpl_vars['cat']->value['config']['icon'];?>
) no-repeat left top;">
                <div><a href="/shop/<?php echo $_smarty_tpl->tpl_vars['cat']->value['seolink'];?>
"><?php echo $_smarty_tpl->tpl_vars['cat']->value['title'];?>
</a></div>
                <?php if ($_smarty_tpl->tpl_vars['cat']->value['subcats']) {?>
                    <div class="subcats">
                        <?php  $_smarty_tpl->tpl_vars['subcat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subcat']->_loop = false;
 $_smarty_tpl->tpl_vars['num'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat']->value['subcats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subcat']->key => $_smarty_tpl->tpl_vars['subcat']->value) {
$_smarty_tpl->tpl_vars['subcat']->_loop = true;
 $_smarty_tpl->tpl_vars['num']->value = $_smarty_tpl->tpl_vars['subcat']->key;
?>
                            <a href="/shop/<?php echo $_smarty_tpl->tpl_vars['subcat']->value['seolink'];?>
"><?php echo $_smarty_tpl->tpl_vars['subcat']->value['title'];?>
</a><?php if ($_smarty_tpl->tpl_vars['num']->value<sizeof($_smarty_tpl->tpl_vars['cat']->value['subcats'])-1) {?>, <?php }?>
                        <?php } ?>
                    </div>
                <?php }?>
            </li>
        <?php } ?>
    </ul>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cfg']->value['show_filter']&&($_smarty_tpl->tpl_vars['items']->value||$_smarty_tpl->tpl_vars['filter']->value)) {?>

<div class="shop_filter_link">
    <a href="javascript:" onclick="$('.shop_filter').toggle()"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_FILTER'];?>
</a> <?php if ($_smarty_tpl->tpl_vars['filter']->value) {?>Найдено товаров: <?php echo $_smarty_tpl->tpl_vars['total']->value;
}?>
</div>

    <div class="shop_filter">

        <div class="filter_body">
            <form action="/shop/<?php echo $_smarty_tpl->tpl_vars['root_cat']->value['seolink'];?>
" method="post">

                <table cellpadding="2" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td colspan="3"><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_PRICE'];?>
</strong></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="filter[pfrom]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value['pfrom'];?>
" style="width:102px"/></td>
                        <td>&mdash;</td>
                        <td><input type="text" name="filter[pto]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value['pto'];?>
" style="width:102px"/></td>
                    </tr>
                    <?php if ($_smarty_tpl->tpl_vars['cfg']->value['show_filter_vendors']&&is_array($_smarty_tpl->tpl_vars['vendors']->value)) {?>
                    <tr>
                        <td colspan="3" style="padding-top:8px;"><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_VENDORS'];?>
:</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <?php  $_smarty_tpl->tpl_vars['vendor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vendor']->_loop = false;
 $_smarty_tpl->tpl_vars['vendor_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vendor']->key => $_smarty_tpl->tpl_vars['vendor']->value) {
$_smarty_tpl->tpl_vars['vendor']->_loop = true;
 $_smarty_tpl->tpl_vars['vendor_id']->value = $_smarty_tpl->tpl_vars['vendor']->key;
?>
                                <div>
                                    <label>
                                        <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['id'];?>
" name="filter[vendors][]" <?php if (in_array($_smarty_tpl->tpl_vars['vendor']->value['id'],$_smarty_tpl->tpl_vars['filter']->value['vendors'])) {?>checked="checked"<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['vendor']->value['title'];?>

                                    </label>
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php }?>
                    <?php  $_smarty_tpl->tpl_vars['char'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['char']->_loop = false;
 $_smarty_tpl->tpl_vars['tid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['chars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['char']->key => $_smarty_tpl->tpl_vars['char']->value) {
$_smarty_tpl->tpl_vars['char']->_loop = true;
 $_smarty_tpl->tpl_vars['tid']->value = $_smarty_tpl->tpl_vars['char']->key;
?>
                        <?php if ($_smarty_tpl->tpl_vars['char']->value['is_filter']) {?>
                            <tr>
                                <td colspan="3" style="padding-top:8px;">
                                    <strong>
                                        <?php echo $_smarty_tpl->tpl_vars['char']->value['title'];
if ($_smarty_tpl->tpl_vars['char']->value['units']) {?>, <?php echo $_smarty_tpl->tpl_vars['char']->value['units'];
}?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <?php if ($_smarty_tpl->tpl_vars['char']->value['fieldtype']!='int') {?>
                                    <td colspan="3">
                                        <?php if ($_smarty_tpl->tpl_vars['char']->value['values']) {?>
                                            <?php if ($_smarty_tpl->tpl_vars['char']->value['is_filter_many']) {?>
                                                <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['vid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['char']->value['values_arr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['vid']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
                                                    <div>
                                                        <label><input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][]" <?php if (in_array(trim($_smarty_tpl->tpl_vars['val']->value),$_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']])) {?>checked="checked"<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</label>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <select name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
]" style="width:100%">
                                                    <option value="" <?php if (!$_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_FILTER_ALL'];?>
</option>
                                                    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['vid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['char']->value['values_arr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['vid']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
                                                        <option value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" <?php if (trim($_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']])==trim($_smarty_tpl->tpl_vars['val']->value)) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</option>
                                                    <?php } ?>
                                                </select>
                                            <?php }?>
                                        <?php } else { ?>
                                                <input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']];?>
" style="width:99%"/>
                                        <?php }?>
                                    </td>
                                <?php } else { ?>
                                    <td><input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][from]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]['from'];?>
" style="width:102px"/></td>
                                    <td>&mdash;</td>
                                    <td><input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][to]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]['to'];?>
" style="width:102px"/></td>
                                <?php }?>
                            </tr>
                        <?php }?>
                    <?php } ?>
                </table>
                <p>
                    <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_FILTER_SUBMIT'];?>
" />
                    <?php if ($_smarty_tpl->tpl_vars['filter']->value) {?><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_FILTER_CANCEL'];?>
" onclick="window.location.href='/shop/<?php echo $_smarty_tpl->tpl_vars['root_cat']->value['seolink'];?>
/all'" /><?php }?>
                    <input type="button" value="Закрыть" onclick="$('.shop_filter').toggle()" />
                </p>
            </form>
        </div>
    </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <?php echo $_smarty_tpl->getSubTemplate ('com_inshop_items-kp.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['filter']->value) {?>
        <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_ITEMS_NOT_FOUND'];?>
</p>
    <?php }?>
<?php }?>

<?php }} ?>
