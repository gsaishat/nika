<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-04-29 01:16:15
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\components\com_catalog_item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:38145903bbb7680db4-26851198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0097f7898dc0321bf117789fe633cb578be95e35' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\components\\com_catalog_item.tpl',
      1 => 1493417722,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38145903bbb7680db4-26851198',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5903bbb786c226_13649027',
  'variables' => 
  array (
    'item' => 0,
    'cat' => 0,
    'shopCartLink' => 0,
    'LANG' => 0,
    'fields' => 0,
    'value' => 0,
    'field' => 0,
    'tagline' => 0,
    'ratingForm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5903bbb786c226_13649027')) {function content_5903bbb786c226_13649027($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['item']->value['plugins_output_before'];?>

<div class="main accii">
<?php if ($_smarty_tpl->tpl_vars['cat']->value['view_type']=='shop'||$_smarty_tpl->tpl_vars['item']->value['can_edit']) {?>
	<div id="shop_toollink_div">
    <?php if ($_smarty_tpl->tpl_vars['cat']->value['view_type']=='shop') {?>
        <?php echo $_smarty_tpl->tpl_vars['shopCartLink']->value;?>

    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['item']->value['can_edit']) {?>
        <a href="/catalog/edit<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
.html" class="uc_item_edit_link"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['EDIT'];?>
</a>
    <?php }?>
    </div>
<?php }?>


<table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:10px; padding: 10px;"><tr>
	<td align="left" valign="top" width="10" class="uc_detailimg">

        <div id="photo-accii">
		<?php if (strlen($_smarty_tpl->tpl_vars['item']->value['imageurl'])>4) {?>
            <a class="lightbox-enabled" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" rel="lightbox" href="/images/catalog/<?php echo $_smarty_tpl->tpl_vars['item']->value['imageurl'];?>
" target="_blank">
                <img alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" src="/images/catalog/medium/<?php echo $_smarty_tpl->tpl_vars['item']->value['imageurl'];?>
" />
            </a>
        <?php } else { ?>
            <img src="/images/catalog/medium/nopic.jpg" border="0" />
        <?php }?>
        </div>

        <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['field']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
            <?php if ($_smarty_tpl->tpl_vars['value']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['field']->value=="Местоположение") {?>
                    </ul><div class="place"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</div>
                <?php }?>
            <?php }?>
        <?php } ?>


    </td>

    <td class="uc_list_itemdesc" align="left" valign="top" class="uc_detaildesc">
        <h1 class="con_heading"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</h1>
        <ul class="chars_list acciya">
			<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['field']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
                <?php if ($_smarty_tpl->tpl_vars['value']->value) {?>
                    <?php if (strstr($_smarty_tpl->tpl_vars['field']->value,'/~l~/')) {?>
                        <li class="uc_detailfield"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</li>
                    <?php } elseif ($_smarty_tpl->tpl_vars['field']->value=="Телефон") {?>
                        <li class="phone"><span class="quest"><?php echo $_smarty_tpl->tpl_vars['field']->value;?>
:</span><span class="answer"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
                    <?php } elseif ($_smarty_tpl->tpl_vars['field']->value=="Описание") {?>
                        </ul><div class="description"><span class="quest"><?php echo $_smarty_tpl->tpl_vars['field']->value;?>
:</span><br><br><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['field']->value=="Местоположение") {?>
                        </ul>
                    <?php } else { ?>
                        <li><span class="quest"><?php echo $_smarty_tpl->tpl_vars['field']->value;?>
:</span><span class="answer"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</span></li>
                    <?php }?>
                <?php }?>
			<?php } ?>
		</ul>
        <br/>
        <a class="print" href="javascript:(print());"><span>Распечатать страницу</span></a>

        <?php if ($_smarty_tpl->tpl_vars['cat']->value['view_type']=='shop') {?>
			<div id="shop_price">
                <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['PRICE'];?>
:</span> <?php echo $_smarty_tpl->tpl_vars['item']->value['price'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['CURRENCY'];?>

            </div>

			<div id="shop_ac_itemdiv">
                <a href="/catalog/addcart<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
.html" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ADD_TO_CART'];?>
" id="shop_ac_item_link">
					<img src="/components/catalog/images/shop/addcart.jpg" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ADD_TO_CART'];?>
"/>
                </a>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['item']->value['on_moderate']) {?>

                <div id="shop_moder_form">
                    <p class="notice"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['WAIT_MODERATION'];?>
:</p>
                    <table cellpadding="0" cellspacing="0" border="0"><tr>
                    <td>
                            <form action="/catalog/moderation/accept<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
.html" method="POST">
                                <input type="submit" name="accept" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['MODERATION_ACCEPT'];?>
"/>
                            </form>
                          </td>
                    <td>
                            <form action="/catalog/edit<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
.html" method="POST">
                                <input type="submit" name="accept" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['EDIT'];?>
"/>
                            </form>
                          </td>
                    <td>
                            <form action="/catalog/moderation/reject<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
.html" method="POST">
                                 <input type="submit" name="accept" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['MODERATION_REJECT'];?>
"/>
                            </form>
                          </td>
                    </tr></table>

                </div>
        <?php }?>

	</td>
</tr></table>

<?php if (($_smarty_tpl->tpl_vars['cat']->value['showtags'])&&($_smarty_tpl->tpl_vars['tagline']->value)) {?>
    <div class="uc_detailtags"><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['TAGS'];?>
: </strong><?php echo $_smarty_tpl->tpl_vars['tagline']->value;?>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cat']->value['is_ratings']) {?>
	<?php echo $_smarty_tpl->tpl_vars['ratingForm']->value;?>

<?php }?>

</div>
<?php echo $_smarty_tpl->tpl_vars['item']->value['plugins_output_after'];?>
<?php }} ?>
