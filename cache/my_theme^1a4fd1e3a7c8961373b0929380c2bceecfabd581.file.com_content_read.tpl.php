<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-04-30 23:00:38
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\components\com_content_read.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29238590642663646f0-31462534%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a4fd1e3a7c8961373b0929380c2bceecfabd581' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\components\\com_content_read.tpl',
      1 => 1492959424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29238590642663646f0-31462534',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'article' => 0,
    'is_pages' => 0,
    'LANG' => 0,
    'cfg' => 0,
    'pt_pages' => 0,
    'tid' => 0,
    'page' => 0,
    'pages' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5906426677a692_17722454',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5906426677a692_17722454')) {function content_5906426677a692_17722454($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['article']->value['plugins_output_before'];?>

<div class="main" style="padding: 20px;">

    <h1 class="con_heading"><?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
</h1>



<?php if ($_smarty_tpl->tpl_vars['is_pages']->value) {?>

	<div class="con_pt" id="pt">
		<span class="con_pt_heading">
			<a class="con_pt_hidelink" href="javascript:void;" onClick="$('#pt_list').toggle();"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['CONTENT'];?>
</a>
			<?php if ($_smarty_tpl->tpl_vars['cfg']->value['pt_hide']) {?> [<a href="javascript:void(0);" onclick="$('#pt').hide();"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['HIDE'];?>
</a>] <?php }?>
		</span>
		<div id="pt_list" style="<?php if ($_smarty_tpl->tpl_vars['cfg']->value['pt_disp']) {?>display: block;<?php } else { ?>display: none;<?php }?> width:100%">
			<div>
				<ul id="con_pt_list">
				<?php  $_smarty_tpl->tpl_vars['pages'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pages']->_loop = false;
 $_smarty_tpl->tpl_vars['tid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['pt_pages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pages']->key => $_smarty_tpl->tpl_vars['pages']->value) {
$_smarty_tpl->tpl_vars['pages']->_loop = true;
 $_smarty_tpl->tpl_vars['tid']->value = $_smarty_tpl->tpl_vars['pages']->key;
?>
					<?php if (($_smarty_tpl->tpl_vars['tid']->value+1!=$_smarty_tpl->tpl_vars['page']->value)) {?>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['pages']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['pages']->value['title'];?>
</a></li>
					<?php } else { ?>
						<li><?php echo $_smarty_tpl->tpl_vars['pages']->value['title'];?>
</li>
					<?php }?>
				<?php } ?>
				<ul>
			</div>
		</div>
	</div>

<?php }?>

<div class="con_text" style="overflow:hidden">

    <?php if ($_smarty_tpl->tpl_vars['article']->value['image']) {?>
        <div class="con_image" style="float:left;margin-top:10px;margin-right:20px;margin-bottom:20px">
            <img src="/images/photos/medium/<?php echo $_smarty_tpl->tpl_vars['article']->value['image'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['article']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
"/>
        </div>
    <?php }?>
    <?php echo $_smarty_tpl->tpl_vars['article']->value['content'];?>

</div>

</div>

<?php }} ?>
