<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-05-01 23:40:53
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\modules\mod_inshop_filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:54495903ba9d2498a8-25601397%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '242f23652d406ef93b77f84ec13fd1b624a5139b' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\modules\\mod_inshop_filter.tpl',
      1 => 1493671251,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '54495903ba9d2498a8-25601397',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5903ba9d40a448_02541938',
  'variables' => 
  array (
    'root_cat' => 0,
    'prices' => 0,
    'price' => 0,
    'cfg' => 0,
    'vendors' => 0,
    'LANG' => 0,
    'vendor' => 0,
    'filter' => 0,
    'chars' => 0,
    'char' => 0,
    'val' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5903ba9d40a448_02541938')) {function content_5903ba9d40a448_02541938($_smarty_tpl) {?><?php echo '<script'; ?>
>
    $(document).ready(function(){
        $("select").selecter();
    });
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">

<?php echo '</script'; ?>
>
<form action="/shop/<?php echo $_smarty_tpl->tpl_vars['root_cat']->value['seolink'];?>
" method="post">
    <div class="filter-item prices">
        <span class="title">Цена, ₽</span>
        <div class="filter-price">
            <div class="filter1">
                <select id="price1" name="filter[pfrom]" placeholder="От">
                    <option value="" selected="selected">От</option>
                    <?php  $_smarty_tpl->tpl_vars['price'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['price']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['price']->key => $_smarty_tpl->tpl_vars['price']->value) {
$_smarty_tpl->tpl_vars['price']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['price']->value;?>
, ₽</option>
                    <?php } ?>
                </select>
            </div>
            <div class="filter2">
                <select id="price2" name="filter[pto]" placeholder="До" >
                    <option value="" selected="selected">До</option>
                    <?php  $_smarty_tpl->tpl_vars['price'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['price']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['price']->key => $_smarty_tpl->tpl_vars['price']->value) {
$_smarty_tpl->tpl_vars['price']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['price']->value;?>
, ₽</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['cfg']->value['show_filter_vendors']&&is_array($_smarty_tpl->tpl_vars['vendors']->value)) {?>
        <span class="title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['SHOP_VENDORS'];?>
:</span>
        <div class="filter">
            <?php  $_smarty_tpl->tpl_vars['vendor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['vendor']->_loop = false;
 $_smarty_tpl->tpl_vars['vendor_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['vendor']->key => $_smarty_tpl->tpl_vars['vendor']->value) {
$_smarty_tpl->tpl_vars['vendor']->_loop = true;
 $_smarty_tpl->tpl_vars['vendor_id']->value = $_smarty_tpl->tpl_vars['vendor']->key;
?>
                <div>
                    <label>
                        <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['vendor']->value['id'];?>
" name="filter[vendors][]" <?php if (in_array($_smarty_tpl->tpl_vars['vendor']->value['id'],$_smarty_tpl->tpl_vars['filter']->value['vendors'])) {?>checked="checked"<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['vendor']->value['title'];?>

                    </label>
                </div>
            <?php } ?>
        </div>
    <?php }?>


    <?php  $_smarty_tpl->tpl_vars['char'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['char']->_loop = false;
 $_smarty_tpl->tpl_vars['tid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['chars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['char']->key => $_smarty_tpl->tpl_vars['char']->value) {
$_smarty_tpl->tpl_vars['char']->_loop = true;
 $_smarty_tpl->tpl_vars['tid']->value = $_smarty_tpl->tpl_vars['char']->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['char']->value['is_filter']) {?>
            <div class="filter-item">
                <span class="title"><?php echo $_smarty_tpl->tpl_vars['char']->value['title'];
if ($_smarty_tpl->tpl_vars['char']->value['units']) {?>, <?php echo $_smarty_tpl->tpl_vars['char']->value['units'];
}?></span>
                <div class="filter">
                    <?php if ($_smarty_tpl->tpl_vars['char']->value['fieldtype']!='int') {?>
                        <?php if ($_smarty_tpl->tpl_vars['char']->value['values']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['char']->value['is_filter_many']) {?>
                                <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['vid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['char']->value['values_arr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['vid']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
                                    <div>
                                        <label><input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][]" <?php if (in_array(trim($_smarty_tpl->tpl_vars['val']->value),$_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']])) {?>checked="checked"<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</label>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <select  name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
]">
                                    <option value="" <?php if (!$_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]) {?>selected="selected"<?php }?>>Выбрать</option>
                                    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['vid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['char']->value['values_arr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['vid']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
" <?php if (trim($_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']])==trim($_smarty_tpl->tpl_vars['val']->value)) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</option>
                                    <?php } ?>
                                </select>
                            <?php }?>
                        <?php } else { ?>
                            <input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']];?>
" style="width:99%"/>
                        <?php }?>
                    <?php } else { ?>
                        <input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][from]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]['from'];?>
" style="width:102px"/>
                        <input type="text" name="filter[<?php echo $_smarty_tpl->tpl_vars['char']->value['id'];?>
][to]" class="input" value="<?php echo $_smarty_tpl->tpl_vars['filter']->value[$_smarty_tpl->tpl_vars['char']->value['id']]['to'];?>
" style="width:102px"/>
                    <?php }?>
                </div>
            </div>
        <?php }?>
    <?php } ?>

    <a href="/shop/doma" class="clear-filter">
        <img class="icon-zoom" src="/templates/my_theme/images/clear.png" />
    </a>

    <div id="clear"></div>
    <div class="button">
        <input type="submit" value="Подобрать" />
          
    </div>
</form>
<?php }} ?>
