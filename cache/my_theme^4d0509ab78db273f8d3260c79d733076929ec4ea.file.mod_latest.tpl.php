<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-04-29 00:56:45
         compiled from "E:\Aishat\Program\OpenServer\domains\nika\templates\my_theme\modules\mod_latest.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206045903ba9d9b27e7-70555039%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d0509ab78db273f8d3260c79d733076929ec4ea' => 
    array (
      0 => 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\templates\\my_theme\\modules\\mod_latest.tpl',
      1 => 1492945555,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206045903ba9d9b27e7-70555039',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cfg' => 0,
    'is_ajax' => 0,
    'module_id' => 0,
    'articles' => 0,
    'article' => 0,
    'LANG' => 0,
    'pagebar_module' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5903ba9dda3c45_39222438',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5903ba9dda3c45_39222438')) {function content_5903ba9dda3c45_39222438($_smarty_tpl) {?><?php if (!is_callable('smarty_function_profile_url')) include 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\includes\\smarty\\libs\\plugins\\function.profile_url.php';
if (!is_callable('smarty_modifier_spellcount')) include 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\includes\\smarty\\libs\\plugins\\modifier.spellcount.php';
if (!is_callable('smarty_modifier_truncate')) include 'E:\\Aishat\\Program\\OpenServer\\domains\\nika\\includes\\smarty\\libs\\plugins\\modifier.truncate.php';
?><?php if ($_smarty_tpl->tpl_vars['cfg']->value['is_pag']) {?>
	<?php echo '<script'; ?>
 type="text/javascript">
		function conPage(page, module_id){
            $('div#module_ajax_'+module_id).css({ opacity:0.4, filter:'alpha(opacity=40)' });
			$.post('/modules/mod_latest/ajax/latest.php', { 'module_id': module_id, 'page':page }, function(data){
				$('div#module_ajax_'+module_id).html(data);
                $('div#module_ajax_'+module_id).css({ opacity:1.0, filter:'alpha(opacity=100)' });
			});
		}
    <?php echo '</script'; ?>
>
<?php }?>
<?php if (!$_smarty_tpl->tpl_vars['is_ajax']->value) {?><div id="module_ajax_<?php echo $_smarty_tpl->tpl_vars['module_id']->value;?>
"><?php }?>

<div id="banner">
<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_smarty_tpl->tpl_vars['aid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['articles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value) {
$_smarty_tpl->tpl_vars['article']->_loop = true;
 $_smarty_tpl->tpl_vars['aid']->value = $_smarty_tpl->tpl_vars['article']->key;
?>
	<div class="mod_latest_entry">
        <?php if ($_smarty_tpl->tpl_vars['article']->value['image']) {?>
            <div class="mod_latest_image">
				<a class="mod_latest_title" href="<?php echo $_smarty_tpl->tpl_vars['article']->value['url'];?>
"><img src="/images/photos/small/<?php echo $_smarty_tpl->tpl_vars['article']->value['image'];?>
" width="260"  alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['article']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
"/></a>
            </div>
        <?php }?>

		<?php if ($_smarty_tpl->tpl_vars['cfg']->value['showdate']) {?>
            <div class="mod_latest_date">
                <?php echo $_smarty_tpl->tpl_vars['article']->value['fpubdate'];?>
 - <a href="<?php echo smarty_function_profile_url(array('login'=>$_smarty_tpl->tpl_vars['article']->value['user_login']),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['article']->value['author'];?>
</a><?php if ($_smarty_tpl->tpl_vars['cfg']->value['showcom']) {?> - <a href="<?php echo $_smarty_tpl->tpl_vars['article']->value['url'];?>
" title="<?php echo smarty_modifier_spellcount($_smarty_tpl->tpl_vars['article']->value['comments'],$_smarty_tpl->tpl_vars['LANG']->value['COMMENT1'],$_smarty_tpl->tpl_vars['LANG']->value['COMMENT2'],$_smarty_tpl->tpl_vars['LANG']->value['COMMENT10']);?>
" class="mod_latest_comments"><?php echo $_smarty_tpl->tpl_vars['article']->value['comments'];?>
</a><?php }?> - <span class="mod_latest_hits"><?php echo $_smarty_tpl->tpl_vars['article']->value['hits'];?>
</span>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['cfg']->value['showdesc']) {?>
            <div class="mod_latest_desc" style="overflow:hidden">
                <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['article']->value['description']),200);?>

            </div>
        <?php }?>
	</div>
<?php } ?>
<?php if ($_smarty_tpl->tpl_vars['cfg']->value['showrss']) {?>
	<div class="mod_latest_rss">
		<a href="/rss/content/<?php if ($_smarty_tpl->tpl_vars['cfg']->value['cat_id']) {
echo $_smarty_tpl->tpl_vars['cfg']->value['cat_id'];
} else { ?>all<?php }?>/feed.rss"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['LATEST_RSS'];?>
</a>
	</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['cfg']->value['is_pag']&&$_smarty_tpl->tpl_vars['pagebar_module']->value) {?>
    <div class="mod_latest_pagebar"><?php echo $_smarty_tpl->tpl_vars['pagebar_module']->value;?>
</div>
<?php }?>
</div>
<?php if (!$_smarty_tpl->tpl_vars['is_ajax']->value) {?></div><?php }?><?php }} ?>
